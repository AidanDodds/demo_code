#pragma once

typedef unsigned char byte;

typedef float (*func_1d)( float );

class cGene
{
	struct sInput
	{
		enum eType
		{
			CONST	= 0,
			IN1		= 1,
			IN2		= 2,
			IN3		= 3
		};

		byte  type;
		float value;
	};
	
	float _eval ( int index ) const;
	void  _print( int index ) const;
	
	void  _copy ( const cGene & b, int index );
	
public:

	byte   opcode[15];
	sInput input [16];

	cGene( void );

	void  clone ( const cGene & in );
	void  breed ( const cGene & a, const cGene & b );
	void  mutate( void );

	void  random_1d  ( void );

	void  print      ( void ) const;
	float evaluate_1d( const float in );
};

class cSpecies
{
	func_1d function;
	float   lo_range;
	float   hi_range;

	static const int nItems = 64;

	float score[ nItems ];
	cGene pool [ nItems ];

	
	void  avoidInbreeding( void );

public:
	
	float getError( cGene &gene );

	void  learn1D
	(
		func_1d		 func	,
		float		 start	,
		float		 end	
	);

	void getBest( cGene &out );
	void refine ( int n );
	void reset  ( void );

};

