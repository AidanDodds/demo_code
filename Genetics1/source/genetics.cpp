#include "genetics.h"
#include "rand.h"
#include <assert.h>
#include <stdio.h>

#define PARENT( X ) (( (X+1) / 2     )-1)
#define LCHILD( X ) (( (X+1) * 2     )-1)
#define RCHILD( X ) (( (X+1) * 2 + 1 )-1)

static const int nOpcodes = 5;

void cGene::_print( int index ) const
{
	if ( index >= 15 )
	{
		int i = index-15;
		assert( i >= 0 && i < 16 );
		switch ( input[i].type )
		{
		case ( sInput::CONST ):
			printf( "%f", input[i].value );
			break;
		case ( sInput::IN1 ):
			putchar( 'x' );
			break;
		}
	}
	else
	{
		putchar( '(' );
		switch ( opcode[ index ] )
		{
		case ( 0 ): // add
			_print( LCHILD( index ) );
			putchar( '+' );
			_print( RCHILD( index ) );
			break;

		case ( 1 ): // sub
			_print( LCHILD( index ) );
			putchar( '-' );
			_print( RCHILD( index ) );
			break;

		case ( 2 ): // mul
			_print( LCHILD( index ) );
			putchar( '*' );
			_print( RCHILD( index ) );
			break;

		case ( 3 ): // div
			_print( LCHILD( index ) );
			putchar( '/' );
			_print( RCHILD( index ) );
			break;

		case ( 4 ): // abs( a ) * b
			printf( "abs(" );
			_print( LCHILD( index ) );
			printf( ")*" );
			_print( RCHILD( index ) );
			break;
		}
		putchar( ')' );
	}
	
}

void cGene::print( void ) const
{
	printf( "float y=" );
	_print( 0 );
	printf( ";                       " );
}

cGene::cGene( void )
{
	for ( int i=0; i<15; i++ )
		opcode[ i ] = 0;

	for ( int i=0; i<16; i++ )
	{
		input[ i ].value = 0.0f;
		input[ i ].type  = sInput::CONST;
	}
}

void cGene::clone( const cGene & in )
{
	int nBytes = sizeof( cGene );

	byte *d = (byte*)this;
	byte *s = (byte*)&in;

	for ( int i=0; i<nBytes; i++ )
		d[i] = s[i];
	
}

void cGene::mutate( void )
{
	int nCount = 1 + randi( 10 );

	for ( int i=0; i < nCount; i++ )
	{
		switch ( randi( 3 ) )
		{
		case ( 0 ):
			input[ randi( 16 ) ].value += gaussf( ) * 4.0f;
			input[ randi( 16 ) ].value += gaussf( ) * 4.0f;
			input[ randi( 16 ) ].value += gaussf( ) * 4.0f;
			break;

		case ( 1 ):
			opcode[ randi( 15 ) ] = randi( nOpcodes );
			break;

		case ( 2 ):
			if ( randf( ) > 0.0f )
				input[ randi( 16 ) ].type = sInput::IN1;
			else
				input[ randi( 16 ) ].type = sInput::CONST;
			break;

		default:
			// this didnt count
			i--;
		}
	}
}

void cGene::_copy( const cGene & b, int index )
{
	if ( index >= 15 )
	{
		int iix = index - 15;
		assert( iix >= 0 && iix < 16 );
		input[ iix ].value = b.input[ iix ].value;
		input[ iix ].type  = b.input[ iix ].type;
	}
	else
	{
		opcode[ index ] = b.opcode[ index ];
		_copy( b, LCHILD( index ) );
		_copy( b, RCHILD( index ) );
	}
}

void cGene::breed( const cGene & a, const cGene & b )
{
	clone( a );

	int nCount = 1+randi( 3 );

	for ( int i=0; i<nCount; i++ )
	{
		_copy( b, randi( 15 ) );
	}
}

float cGene::_eval( int index ) const
{
	// if we have reached the input layer
	if ( index >= 15 )
	{
		int iix = index - 15;
		assert( iix >= 0 && iix < 16 );
		return input[ iix ].value;
	}

	// evaluate child values
	float a = _eval( LCHILD( index ) );
	float b = _eval( RCHILD( index ) );

	// evaluate the opcode
	switch ( opcode[ index ] )
	{
	case ( 0 ):	// add
							return a + b;

	case ( 1 ):	// sub
							return a - b;

	case ( 2 ):	// mul
							return a * b;

	case ( 3 ):	// div
			if ( b == 0.f )	return 0.0f;
							return a / b;

	case ( 4 ): // abs( a ) * b
			if ( a < 0 )	return -a * b;
							return  a * b;

//	case ( 9 ): // cmp
//			if ( a < b )	return 1.0f;
//							return 0.0f;

//	case ( 10 ): // cmp
//			if ( a > b )	return 1.0f;
//							return 0.0f;

//	case ( 11 ): // cmp
//			if ( a > b )	return  1.0f;
//							return -1.0f;

//	case ( 12 ): // max
//			if ( a > b )	return  a;
//							return  b;

//	case ( 13 ): // min
//			if ( a < b )	return  a;
//							return  b;

//	case ( 14 ):
//			return 1.0f;

	};

	//
	return 1.f;
}

float cGene::evaluate_1d( const float x )
{
	// deploy inputs
	for ( int i=0; i<16; i++ )
	{
		sInput &in = input[i];
		if ( in.type == sInput::IN1 )
			in.value = x;
	}
	// evaluate the root node
	return _eval( 0 );
}

void cGene::random_1d( void )
{
	for ( int i=0; i<15; i++ )
		opcode[ i ] = randi( nOpcodes );

	for ( int i=0; i<16; i++ )
	{
		if ( randi() & 1 )
			input[ i ].type  = sInput::IN1;
		else
		{
			input[ i ].value = gaussf() * 8.f;
			input[ i ].type  = sInput::CONST;
		}
	}
}

float cSpecies::getError
(
	cGene &gene
)
{
	float error = 0.0f;
	float scale = 0.01f;
	float step  = (hi_range-lo_range) * scale;

	for ( float x=lo_range; x<hi_range; x+=step )
	{
		float m = function( x );
		float a = gene.evaluate_1d( x );
		float d = m - a;

		if ( d > 0.f ) error += d;
		else		   error -= d;
	}

	return error * scale;
}

void cSpecies::learn1D
(
	func_1d		 func	,
	float		 start	,
	float		 end	
)
{
	function = func;
	lo_range = start;
	hi_range = end;
	reset( );
}

void cSpecies::avoidInbreeding( void )
{
	for ( int i=0; i<nItems; i++ )
		for ( int j=0; j<nItems; j++ )
		{
			if ( i==j ) continue;
			
			cGene &a = pool[ i ];
			cGene &b = pool[ j ];

			for ( int k=0; k<15; k++ )
			{
				if ( a.opcode[k] != b.opcode[k] )
					goto next;
			}

			if ( score[i] > score[j] )
			{
				a.random_1d( );
				score[i] = getError( a );
			}
			else
			{
				b.random_1d( );
				score[j] = getError( b );
			}
next:
			;
		}
}

void cSpecies::refine( int n )
{
	// go through some generations
	for ( int i=0; i<n; i++ )
	{
		cGene newGene;

		if ( i % 50 == 0 )
			avoidInbreeding( );

		switch ( randi( 3 ) )
		{
		case ( 0 ): // random
			{
				int a = randi( nItems );
				newGene.random_1d( );
				float gs = cSpecies::getError( newGene );
				if ( score[ a ] > gs )
				{
					pool [ a ].clone( newGene );
					score[ a ] = gs;
				}
			}
			break;

		case ( 1 ):
			{
				int a = randi( nItems );
				newGene.clone( pool[ a ] );
				newGene.mutate( );
				float gs = cSpecies::getError( newGene );

				if ( score[ a ] > gs )
				{
					pool [ a ].clone( newGene );
					score[ a ] = gs;
				}
			}
			break;

		case ( 2 ):
			{
				int a, b;

				do
				{
					a = randi( nItems );
					b = randi( nItems );

					newGene.breed( pool[ a ], pool[ b ] );
					float gs = cSpecies::getError( newGene );

					if ( score[ a ] > gs )
					{
						pool [ a ].clone( newGene );
						score[ a ] = gs;
					}

				} while ( a == b );
			}
			break;

		default:
			continue;
		}
	}
}

void cSpecies::getBest( cGene &out )
{
	int   best = 0;
	float berr = score[ best ];
	for ( int j=0; j<nItems; j++ )
	{
		if ( score[ j ] < berr  )
		{
			berr = score[ best ];
			best = j;
		}
	}

	out.clone( pool[ best ] );
}

void cSpecies::reset( void )
{
	// populate pool
	for ( int i=0; i<nItems; i++ )
	{
		pool [i].random_1d( );
		score[i] = getError( pool[i] );
	}
}