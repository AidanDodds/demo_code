#pragma comment( lib, "sdl.lib" )
#include <math.h>
#include <SDL/SDL.h>
#include "genetics.h"
#include "rand.h"
#include <Windows.h>

extern SDL_Surface *screen = nullptr;

const float pi = 3.14159265359f;

static cSpecies species;
static bool pause = true;

namespace app
{
	void setCaption( void )
	{
	}

	void quit( void )
	{
		if ( screen != NULL )
			SDL_FreeSurface( screen );
		
		SDL_Quit( );
	}

	void init( void )
	{
		atexit( app::quit );

		if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
			exit( -1 );

		screen = SDL_SetVideoMode( 512, 256, 32, 0 );
		if ( screen == NULL )
			exit( -1 );

		setCaption( );
	}

	bool tick( void )
	{
		SDL_Event event;
		while ( SDL_PollEvent( &event ) )
		{
			if ( event.type == SDL_QUIT )
				return false;

			if ( event.type == SDL_KEYDOWN )
			{
				switch ( event.key.keysym.sym )
				{
				case ( SDLK_q ):
				case ( SDLK_ESCAPE ):
					return false;

				case ( SDLK_r ):
					species.reset( );
					break;

				case ( SDLK_p ):
					pause = !pause;

					if ( pause )
						SDL_WM_SetCaption( "Paused", nullptr );
					else
						SDL_WM_SetCaption( "Running", nullptr );

					break;

				}
			}
		}
		return true;
	}

	void plot( int x, int y, int colour )
	{
		if ( x<0 || y<0 || x>=screen->w || y>=screen->h )
			return;

		int *p = (int*) screen->pixels;

		p[ x + y * screen->w ] = colour;
	}

	void cls( void )
	{
		SDL_FillRect( screen, nullptr, 0x1f1f1f );
	}

};

float myFunc( float in )
{
	return sinf( in );
}

int main( int argc, char **args )
{
	SDL_WM_SetCaption( "Paused", nullptr );

	seedRand( SDL_GetTicks( ) );

	app::init( );
	
	const float step  = 0.003f;

	cGene best;

	species.learn1D( myFunc, -pi*2, pi*2 );

	int shw = screen->w / 2;
	int shh = screen->h / 2;

	while ( app::tick( ) )
	{
		app::cls( );
		
		if (! pause )
		{
			species.refine( 500 );

			species.getBest( best );

			COORD zero = { 0, 0 };
			SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), zero );

			printf( "// %f \n", species.getError( best ) );
			best.print( );
		}
		else
			Sleep( 100 );
		
		for ( float x=-1; x<1; x += step )
		{
			float a = myFunc          ( x * pi );
			float c = best.evaluate_1d( x * pi );

			int xp = shw + x * shw;

			app::plot( xp, shh - shh * a, 0x0000FF );
			app::plot( xp, shh - shh * c, 0x00FF00 );
		}

		SDL_Flip( screen );

	}

	return 0;
}