#pragma once

typedef   signed int s32;
typedef unsigned int u32;

extern u32 _useed;
extern s32 _sseed;

// info	: provide the random number seed
inline void seedRand( u32 seed )
{
	_sseed = _useed = seed;
}

// info	: generic unsigned random number generator
// out	: unsigned integer 0 to 0xFFFF
inline u32 randi( void )
{
	_useed *= 1103515245;
	_useed += 12345;
	return _useed >> 15;
}

// info	: constrained random number
inline u32 randi( u32 max )
{
	return randi() % max;
}

// info  : gaussian distribution random number
// out   : float -1.0f to +1.0f 
inline float gaussf( void )
{
	// scale factor
	float scale  = 0.125;			//				1.0f / taps		( 8.0 )
	      scale *= 0.0000305185f;	//				1.0f / rand_max	( 0x7FFF )
	      scale *= 1.22474487139f;	// std_deviat..	1.0f / ( sqrt( 8.0f / 12.0f ) )
	// glibc constants
	const int c1 = 1103515245;
	const int c2 = 12345;
	// 
	s32 &s  = _sseed;
	s32  a  = ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	     a += ( s = s * c1 + c2 ) >> 16;
	// 
	return (float) a * scale;
}

// 
inline float randf( void )
{
	float scale = 0.0000305185f;
	const int c1 = 1103515245;
	const int c2 = 12345;
	s32 &s  = _sseed;
	s32  a  = ( s = s * c1 + c2 ) >> 16;
	return (float) a * scale;
}