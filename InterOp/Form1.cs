﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Timers;

namespace InterOp
{
    public partial class TestForm : Form
    {
        System.Timers.Timer _timer;
        const int fps = 50;

        public void onTick( object source, ElapsedEventArgs e )
        {
            panel1.Invalidate();
            panel2.Invalidate();
            panel3.Invalidate();
        }

        public TestForm()
        {
            InitializeComponent();
            _timer = new System.Timers.Timer(1000 / fps);
            _timer.Elapsed += onTick;
            _timer.Start();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            if (!myDll.init())
            {
                MessageBox.Show("Unable to load client dll!");
                Application.Exit();
            }
        }

        private void TestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            myDll.free( );
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            myDll.dSurfaceDraw(panel1.Handle,0xff0000);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            myDll.dSurfaceDraw(panel2.Handle, 0x00ff00);
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            myDll.dSurfaceDraw(panel3.Handle, 0x0000ff);
        }

    }

    public class myDll
    {
        static bool dllLoaded = false;

        public static bool init()
        {
            if (!dllLoaded)
            {
                try
                {
                    if (dInit() >= 0)
                    {
                        dllLoaded = true;
                        return true;
                    }
                }
                catch (DllNotFoundException)
                {
                    dllLoaded = false;
                }
            }
            return false;
        }

        public static void paint(IntPtr window, int windowID)
        {
            if ( dllLoaded )
                dSurfaceDraw(window, windowID);
        }

        public static void free( )
        {
            if ( dllLoaded )
                dSurfaceFreeAll();
        }

        [DllImport("myDll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "dll_init")]
        public static extern int dInit();

        [DllImport("myDll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "dll_surfaceDraw")]
        public static extern void dSurfaceDraw(IntPtr window, Int32 windowID);

        [DllImport("myDll.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "dll_surfaceFreeAll")]
        public static extern void dSurfaceFreeAll();
    }

}
