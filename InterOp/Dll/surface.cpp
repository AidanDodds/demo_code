#include <Windows.h>
#include <intrin.h>

#include "surface.h"

//----------------------------------------
// simple spin lock
//----------------------------------------
struct sLock
{
    sLock( void )
        : _var( 0 )
    { }

    volatile long _var;

    void lock( void )
    {
        while ( _InterlockedExchange( &_var, 1 ) == 1 );
    }

    bool tryLock( void )
    {
        return _InterlockedExchange( &_var, 1 ) == 0;
    }

    void unlock( )
    {
        _InterlockedExchange( &_var, 0 );
    }
};

//----------------------------------------
// surface data container
//----------------------------------------
struct sSurface
{
    sSurface( void )
        : _pixels   ( nullptr )
        , next      ( nullptr )
    { }

    void    *_pixels;
	HWND	 _hwnd;
	HBITMAP	 _bmp;
    HDC      _dcMem;
	RECT	 _rect;

    // form a linked list
    sSurface *next;
};

sLock listLock;
static sSurface *list = nullptr;

//----------------------------------------
// find a surface in the list
//----------------------------------------
sSurface * surface_find
    ( HWND window )
{
    listLock.lock( );
    {
        sSurface *s = list;
        while ( s != nullptr )
        {
            if ( s->_hwnd == window )
                break;

            s = s->next;
        }
    }
    listLock.unlock( );
    return s;
}

//----------------------------------------
// release all surfaces in the list
//----------------------------------------
void surface_freeAll
    ( void )
{
    listLock.lock( );
    {
        sSurface *s = list;
        while ( s != nullptr )
        {
            // release the DC for the bitmap
	        if ( s->_dcMem != NULL )
	        {
		        DeleteDC( s->_dcMem );
		        s->_dcMem = NULL;
	        }
	        // release the bitmap
	        if ( s->_bmp != NULL )
	        {
		        DeleteObject( s->_bmp );
		        s->_bmp = NULL;
            }
            // delete this node
            sSurface *t = s;
            s = s->next;
            delete t;
        }
        // list is empty
        list = nullptr;
    }
    listLock.unlock( );
}

//----------------------------------------
// create a new surface
//----------------------------------------
sSurface *surface_create
    ( HWND window )
{
    sSurface *s = new sSurface;
    s->_hwnd = window;

	// get client surface size
	if ( GetClientRect( window, &s->_rect ) == FALSE )
		goto onError;

	// get the device context of surface
	HDC _dc = GetDC( window );
	if ( _dc == nullptr )
		goto onError;

	// create a new dc compatable with the surface
	s->_dcMem = CreateCompatibleDC( _dc );

	// release the DC now we have a compatable one
	ReleaseDC( window, _dc );

	// quit if we dont have a compatable DC
	if ( s->_dcMem == nullptr )
		goto onError;

	// create a bitmap for the surface
	BITMAPINFO info =
	{
		{
			sizeof( BITMAPINFOHEADER ),
			s->_rect.right,
			s->_rect.bottom,
			1,
			32,						// 32bit
			BI_RGB,					// packed RGB
			0,
			1,						// pixel per metre
			1,						// pixel per metre
			0,
			0
		},
		0
	};
	s->_bmp = CreateDIBSection
	(
		s->_dcMem,
		&info,
		DIB_RGB_COLORS,
		&s->_pixels,
		0,
		0
	);

	// check if we have a bitmap
	if ( s->_bmp == NULL )
	{
		DeleteDC( s->_dcMem );
		goto onError;
	}
    
    listLock.lock( );
    {
        // add to linked list
        s->next = list;
        list = s;
    }
    listLock.unlock( );

    // return success
    return s;

    // failed
onError:
    delete s;
    return nullptr;
}

//----------------------------------------
// flip the pixel buffer to the surface
//----------------------------------------
void surface_flip
    ( sSurface *s )
{
    if ( s == nullptr )
        return;

	// flip the bitmap to the screen
	if ( s->_dcMem && s->_bmp )
	{
		GdiFlush();
		SelectObject( s->_dcMem, s->_bmp );
		BitBlt
            (
                GetDC( s->_hwnd ),
                0,
                0,
                s->_rect.right,
                s->_rect.bottom,
                s->_dcMem,
                0,
                0,
                SRCCOPY
            );
	}
}

//----------------------------------------
// flip the pixel buffer to the surface
//----------------------------------------
bool surface_access
    ( sSurface *s, unsigned int *&pix, int &w, int &h )
{
    if ( s == nullptr )
        return false;

    // grab values from the structure
    pix = (unsigned int *) s->_pixels;
    w   = s->_rect.right;
    h   = s->_rect.bottom;

    return true;
}
