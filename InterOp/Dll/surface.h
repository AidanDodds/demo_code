#pragma once

// forward declare the surface structure
struct sSurface;

//----------------------------------------
// find a surface in the list
//----------------------------------------
sSurface *surface_find
    ( HWND window );

//----------------------------------------
// release all surfaces in the list
//----------------------------------------
void surface_freeAll
    ( void );

//----------------------------------------
// create a new surface
//----------------------------------------
sSurface *surface_create
    ( HWND window );

//----------------------------------------
// flip the pixel buffer to the surface
//----------------------------------------
void surface_flip
    ( sSurface *s );

//----------------------------------------
// access the raw pixels
//----------------------------------------
bool surface_access
    ( sSurface *s, unsigned int *&pix, int &w, int &h );
