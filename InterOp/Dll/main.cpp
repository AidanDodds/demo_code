#include <Windows.h>
#include "surface.h"

#define DLLEXPORT extern __declspec( dllexport )

//----------------------------------------
// sample render function
//----------------------------------------
static
void _render( sSurface *s, int mask )
{
    int w, h;
	unsigned int *p = nullptr;

    // access pixels of the surface
    if (! surface_access( s, p, w, h ) )
        return;

    // plot random noise
	static unsigned int _r = 0xDEADBEEF;
	for ( int i=0; i<w*h; i++ )
    {
        unsigned int &v = *(p++);

		_r *= 349786197;
		_r += 876518731;
        v   = (_r >> 16) & 0xff;

		_r *= 349786197;
		_r += 876518731;
        v  |= (_r >>  8) & 0xff00;    

		_r *= 349786197;
		_r += 876518731;
        v  |= (_r >>  0) & 0xff0000;    

		v &= mask;
	}
}

extern "C"
{

	DLLEXPORT int _stdcall DllMain( int a, int b, int c )
	{
		// accept an attachment
		return 1;
	}

    DLLEXPORT int _cdecl dll_init( void )
    {
        return 1;
    }

	DLLEXPORT void _cdecl dll_surfaceFreeAll( void )
	{
        surface_freeAll( );
	}

	DLLEXPORT void _cdecl dll_surfaceDraw( HWND window, int windowID )
	{
        // find the surface
        sSurface *s = surface_find( window );

        // create new surface
        if ( s == nullptr )
            s = surface_create( window );

        if ( s != nullptr )
        {
		    // render to the pixel buffer
		    _render( s, windowID );

            // flip this pixel buffer to the screen
            surface_flip( s );
        }
	}

};