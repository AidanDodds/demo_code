#pragma once

typedef   signed long long s64;
typedef unsigned long long u64;
typedef   signed int	   s32;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char      byte;

#define NULL 0

const float C_PI  = 3.14159265359f;
const float C_PI2 = 3.14159265359f * 2.0f;
