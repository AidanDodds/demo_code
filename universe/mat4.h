#pragma once

#include <math.h>
#include "vec3.h"

struct cMat4
{
	void identity( void )
	{
		e[0x0]=1; e[0x1]=0; e[0x2]=0; e[0x3]=0;
		e[0x4]=0; e[0x5]=1; e[0x6]=0; e[0x7]=0;
		e[0x8]=0; e[0x9]=0; e[0xa]=1; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

	void scale( float s )
	{
		e[0x0]=s; e[0x1]=0; e[0x2]=0; e[0x3]=0;
		e[0x4]=0; e[0x5]=s; e[0x6]=0; e[0x7]=0;
		e[0x8]=0; e[0x9]=0; e[0xa]=s; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

	void rotateX( float a )
	{
		float s = sinf( a );
		float c = cosf( a );
		e[0x0]=1; e[0x1]=0; e[0x2]=0; e[0x3]=0;
		e[0x4]=0; e[0x5]=c; e[0x6]=s; e[0x7]=0;
		e[0x8]=0; e[0x9]=-s;e[0xa]=c; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

	void rotateY( float a )
	{
		float s = sinf( a );
		float c = cosf( a );
		e[0x0]=c; e[0x1]=0; e[0x2]=-s;e[0x3]=0;
		e[0x4]=0; e[0x5]=1; e[0x6]=0; e[0x7]=0;
		e[0x8]=s; e[0x9]=0; e[0xa]=c; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

	void rotateZ( float a )
	{
		float s = sinf( a );
		float c = cosf( a );
		e[0x0]=c; e[0x1]=s; e[0x2]=0; e[0x3]=0;
		e[0x4]=-s;e[0x5]=c; e[0x6]=0; e[0x7]=0;
		e[0x8]=0; e[0x9]=0; e[0xa]=1; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

	void translate( const vec3 &in )
	{
		e[0x3] = in.x;
		e[0x7] = in.y;
		e[0xb] = in.z;
	}

	void projection( float fovy, float aspect, float znear, float zfar )
	{
		float r = (fovy / 2) * 57.295779f;
		float deltaZ = zfar - znear;
		float s = sinf(r);

		if (deltaZ == 0 || s == 0 || aspect == 0)
			return;

		float cotangent = cosf(r) / s;

		float i =  cotangent / aspect;
		float j =  cotangent;
		float k = -(zfar + znear) / deltaZ;
		float l = -1;
		float m = -2 * znear * zfar / deltaZ;
		float n =  0;

		e[0x0] = i;	e[0x1] = 0; e[0x2] = 0; e[0x3] = 0;
		e[0x4] = 0; e[0x5] =-j; e[0x6] = 0; e[0x7] = 0;
		e[0x8] = 0; e[0x9] = 0; e[0xa] = k; e[0xb] = l;
		e[0xc] = 0; e[0xd] = 0; e[0xe] = m; e[0xf] = n;
	}

#if 0
	void spherical( float angle, float elevation )
	{
		cVec3  z( angle, elevation );
			   z.fastNorm( );

		cVec3  y(-z.y*z.x, 1-z.y*z.y,-z.y*z.z );
			   y.fastNorm( );

		cVec3  x( z, y );
			   x.fastNorm( );

		e[0x0]=x.x; e[0x1]=y.x; e[0x2]=z.x; 
		e[0x4]=x.y; e[0x5]=y.y; e[0x6]=z.y; 
		e[0x8]=x.z; e[0x9]=y.z; e[0xa]=z.z;
	}
#endif

	void transpose( void )
	{
//		___ 0x1 0x2 0x3
//		0x4 ___ 0x6 0x7
//		0x8 0x9 ___ 0xb
//		0xc 0xd 0xe ___

		float t = 0.0f;
		t=e[0x1]; e[0x1]=e[0x4]; e[0x4]=t; // 1 -> 4
		t=e[0x2]; e[0x2]=e[0x8]; e[0x8]=t; // 2 -> 8
		t=e[0x3]; e[0x3]=e[0xc]; e[0xc]=t; // 3 -> c
		t=e[0x6]; e[0x6]=e[0x9]; e[0x9]=t; // 6 -> 9
		t=e[0x7]; e[0x7]=e[0xd]; e[0xd]=t; // 7 -> d
		t=e[0xb]; e[0xb]=e[0xe]; e[0xe]=t; // b -> e
	}

	void move( float x, float y, float z )
	{
		e[0x3] += e[0x0]*x + e[0x1]*y + e[0x2]*z;
		e[0x7] += e[0x4]*x + e[0x5]*y + e[0x6]*z;
		e[0xb] += e[0x8]*x + e[0x9]*y + e[0xa]*z;
	}
	
	float operator [] ( int i ) const
	{
		return e[ i ];
	}

	static void multiply( const cMat4 &a, const cMat4 &b, cMat4 &d )
	{
		const float *m1Ptr = & a.e[0];
		const float *m2Ptr = & b.e[0];
		      float *dmPtr = & d.e[0];
		
		for ( int i = 0; i < 4; i++ ) 
		{
			for ( int j = 0; j < 4; j++ )
			{
				*dmPtr = m1Ptr[0] * m2Ptr[ 0 * 4 + j ]
					   + m1Ptr[1] * m2Ptr[ 1 * 4 + j ]
					   + m1Ptr[2] * m2Ptr[ 2 * 4 + j ]
					   + m1Ptr[3] * m2Ptr[ 3 * 4 + j ];
				dmPtr++;
			}
			m1Ptr += 4;
		}
	}

	static cMat4 multiply( const cMat4 &a, const cMat4 &b )
	{
		cMat4 d;

		const float *m1Ptr = & a.e[0];
		const float *m2Ptr = & b.e[0];
		      float *dmPtr = & d.e[0];
		
		for ( int i = 0; i < 4; i++ ) 
		{
			for ( int j = 0; j < 4; j++ )
			{
				*dmPtr = m1Ptr[0] * m2Ptr[ 0 * 4 + j ]
					   + m1Ptr[1] * m2Ptr[ 1 * 4 + j ]
					   + m1Ptr[2] * m2Ptr[ 2 * 4 + j ]
					   + m1Ptr[3] * m2Ptr[ 3 * 4 + j ];
				dmPtr++;
			}
			m1Ptr += 4;
		}
		return d;
	}

	vec3 getXVector( void )
	{
		cVec3 t( e[0x0], e[0x4], e[0x8] );
		return t;
	}

	vec3 getYVector( void )
	{
		cVec3 t( e[0x1], e[0x5], e[0x9] );
		return t;
	}

	vec3 getZVector( void )
	{
		cVec3 t( e[0x2], e[0x6], e[0xa] );
		return t;
	}

	float e[ 16 ];

};

typedef cMat4 mat4;

static inline
vec3 operator * ( const vec3 &a, mat4 &b )
{
    return vec3
    (
        a.x * b[0] + a.y * b[1] + a.z * b[2],
        a.x * b[4] + a.y * b[5] + a.z * b[6],
        a.x * b[8] + a.y * b[9] + a.z * b[10]
    );
}