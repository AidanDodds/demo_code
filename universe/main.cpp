#define _SDL_main_h
#include <SDL/SDL.h>
#include <math.h>

#include "vec3.h"
#include "mat4.h"

#pragma comment( lib, "SDL.lib" )

typedef   signed int s32;
typedef unsigned int u32;

const float res_x = 1024;
const float res_y = 768;

namespace app
{

    static SDL_Surface *screen = nullptr;
    
    void plot( int x, int y, u32 c )
    {
         if ( x<0 || y<0 ||x>res_x || y>res_y )
            return;
         u32 *p = (u32*) screen->pixels;
         p[ x + y * screen->w ] = c;
    }

    void init( void )
    {
	    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		    exit( -1 );
	    screen = SDL_SetVideoMode( res_x, res_y, 32, 0 );
	    if ( screen == NULL )
		    exit( -2 );
    }

    bool tick( void )
    {
	    static bool active = true;

	    SDL_Flip( screen );
        SDL_FillRect( screen, nullptr, 0x101010 );

	    SDL_Event event;
	    while ( SDL_PollEvent( &event ) )
	    {
		    if ( event.type == SDL_QUIT )
			    active = false;

		    if ( event.type == SDL_KEYDOWN )
		    {
			    switch ( event.key.keysym.sym )
			    {
			    case ( SDLK_ESCAPE ):
			    case ( SDLK_q ):
				    active = false;
				    break;
			    }
		    }
	    }

	    return active;
    }

};

s32 _sseed;
u32 _useed;

// info  : gaussian distribution random number
// out   : float -1.0f to +1.0f 
static inline
float gaussf( void )
{
	// scale factor
	float scale  = 0.125;			//				1.0f / taps		( 8.0 )
	      scale *= 0.0000305185f;	//				1.0f / rand_max	( 0x7FFF )
	      scale *= 1.22474487139f;	// std_deviat..	1.0f / ( sqrt( 8.0f / 12.0f ) )
	// glibc constants
	const int c1 = 1103515245;
	const int c2 = 12345;
	// 
	s32 &s  = _sseed;
	s32  a  = ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	        a += ( s = s * c1 + c2 ) >> 16;
	// 
	return (float) a * scale;
}

inline u32 randi( void )
{
	_useed *= 1103515245;
	_useed += 12345;
	return _useed >> 15;
}

struct sBody
{
    vec3  p;
    u32   c;
};

const int nBodies = 2000;
sBody body[ nBodies ];
float r = 0.0f;

float absf( float in )
{
    return in > 0.0f ? in : -in;
}

/*
float clampf( float min, float in, float max )
{
    return in > max ? max : (in < min ? min : in);
}
*/

void makeBodies( void )
{
    for ( int i=0; i<nBodies; i++ )
    {
        sBody & b = body[ i ];

        b.p = vec3( gaussf( ), gaussf( ), gaussf( ) );

//        float d = sqrtf( absf( b.p.x * b.p.z ) );
        float d = sqrtf( b.p.x*b.p.x + b.p.z*b.p.z );
        float s = clampf( 0.f, .6f-d, 1.0f );

        b.p.y *= s*s*s;

        b.c = randi( )&0xff;
        b.c = (b.c << 16) | (b.c << 8) | b.c;
    }
}

void drawBodies( void )
{
    mat4 ra; ra.rotateY( r );
    mat4 rb; rb.rotateZ( 0.2f  );
    mat4 rc; rc.rotateX(-0.3f  );

    mat4 u;
    
        mat4::multiply( rb, rc, u );
    u = mat4::multiply(  u, ra );

    for ( int i=0; i<nBodies; i++ )
    {
        sBody & b = body[ i ];

        vec3 p = (b.p * u);

        float x = p.x;
        float y = p.y;

        app::plot( 512 + (int)(x*512), 384 + (int)(y*512), b.c );
    }
}

int main( int argc, char **args )
{
    const float pi2 = 2.0f * 3.14159265359f;

	app::init( );

    makeBodies( );

	// main loop
	while ( app::tick( ) )
	{
        
        drawBodies( );

        r += 0.0002f;
        if ( r > pi2 )
            r -= pi2;
	}
	
	SDL_Quit( );
	return 0;
}