#pragma once

#include <math.h>
#include "utility.h"
#include "types.h"

struct cVec3
{
	float x, y, z;

	// default, zero vector
	cVec3( )
		: x( 0 ), y( 0 ), z( 0 )
	{ }

	// construct from components
	cVec3( float _tx, float _ty, float _tz )
		: x( _tx ), y( _ty ), z( _tz )
	{ }

	// construct from spherical coords
	cVec3( float angle, float elevation )
	{
		spherical( angle, elevation );
	}

	// construct from cross product
	cVec3( cVec3 &a, cVec3 &b )
	{
		cVec3 t = cross( a, b );
		x = t.x;
		y = t.y;
		z = t.z;
	}

	// fast normalize
	cVec3 fastNorm( void )
	{
		float d = isqrtf( x*x + y*y + z*z );
		return cVec3( x *= d, y *= d, z *= d );
	}

	// add, in place
	void operator += ( const cVec3 &a )
	{
		x += a.x; y += a.y; z += a.z;
	}

	// sub, in place
	void operator -= ( const cVec3 &a )
	{
		x -= a.x; y -= a.y; z -= a.z;
	}
		
	//
	void operator *= ( float s )
	{
		x *= s; y *= s; z *= s;
	}
		
	// subtract
	cVec3 operator - ( const cVec3 &a ) const
	{
		return cVec3( x - a.x, y - a.y, z - a.z );
	}

	// addition
	cVec3 operator + ( const cVec3 &a ) const
	{
		return cVec3( x + a.x, y + a.y, z + a.z );
	}

	// dot product
	float operator * ( const cVec3 &a ) const
	{
		return x*a.x + y*a.y + z*a.z;
	}

	// scale
	cVec3 operator * ( const float s ) const
	{
		return cVec3( x*s, y*s, z*s );
	}

	// 
	void scale( const cVec3 &s )
	{
		x *= s.x;
		y *= s.y;
		z *= s.z;
	}

	// real length
	float len( void ) const
	{
		return 1.0f / isqrtf( lenSqr() );
	}

	// return faster, squared length
	float lenSqr( void ) const
	{
		return x*x + y*y + z*z;
	}

	//
	void set( float u, float v, float w )
	{
		x=u; y=v; z=w;
	}

	//
	cVec3 negate( void ) const
	{
		return cVec3( -x, -y, -z );
	}

	// find the normal pointing from 's' to 'd'
	static cVec3 findNormal( const cVec3 &s, const cVec3 &d )
	{
		cVec3 n = d - s;
		n.fastNorm( );
		return n;
	}

	// return squared distance between two vectors
	static float distSqr( const cVec3 &s, const cVec3 &d )
	{
		cVec3 temp = d - s;
		return temp.lenSqr( );
	}

	// reflect vector 'r' around normal 'n'
	static cVec3 reflect( const cVec3 &n, const cVec3 &r )
	{
		return r - n * ( 2 * (r * n) );
	}

	// dot product of two vectors
	static float dot( const cVec3 &a, const cVec3 &b )
	{
		return b.x*a.x + b.y*a.y + b.z*a.z;;
	}
		
	// vector cross product
	static cVec3 cross( const cVec3 &a, const cVec3 &b )
	{
		return cVec3
		(
			a.y * b.z - b.y * a.z,
			a.z * b.x - b.z * a.x,
			a.x * b.y - b.x * a.y
		);
	}
		
	// project vector 'a' onto vector 'b'
	static cVec3 project( const cVec3 &a, const cVec3 &b )
	{
		return b * ((a * b) / b.len( ));
	}
		
	// generate a random vector in the unit sphere
	static cVec3 random( void )
	{
		cVec3 norm;
		do
			norm.set( srandf( ), srandf( ), srandf( ) );
		// reject normals that lie outside of unit sphere
		while ( norm.lenSqr( ) > 1.0f );
		return norm;
	}

	// generate a random unit vector
	static cVec3 randomNormal( void )
	{
		cVec3 norm = random( );
		// convert to unit vector
		norm.fastNorm( );
		return norm;
	}

	// generate a gauss distributed point
	// centering on { 0, 0, 0 }
	static cVec3 randomGauss( void )
	{
		cVec3 norm;
		for ( int i=0; i<8; i++ )
			norm += random( );

		norm *= (1.0f / 8.0f);
		norm *= 1.22474487139f;
	}

	// to produce a gauss weighted normal in the hemesphere of [normal]
	// [scale] (0 to 1) controls the divergence from [normal]
	static cVec3 randomHemesphere( const cVec3 &normal, float scale ) 
	{
		cVec3  temp = normal + (random() * scale);
			   temp.fastNorm( );
		return temp;
	}

	// generate a unit vector from spherical coordinates
	void cVec3::spherical( float azimuth, float elv )
	{
		float sa = sin( azimuth );
		float ca = cos( azimuth );
		float se = sin( elv );
		float ce = cos( elv );
		x = se * ca;
		y = se * sa;
		z = ce;
	}

/*
	void rotateX( float a )
	{
		float c  = cosf( a );
		float s  = sinf( a );
		float ty = y * c - z * s;
			   z = y * s + z * c;
			   y = ty;
	}

	void rotateY( float a )
	{
		float c  = cosf( a );
		float s  = sinf( a );
		float tz = z * c - x * s;
			   x = z * s + x * c;
			   z = tz;
	}

	void rotateZ( float a )
	{
		float c  = cosf( a );
		float s  = sinf( a );
		float tx = x * c - y * s;
			   y = x * s + y * c;
			   x = tx;
	}
*/

	// reflect this vector around normal aVec
	void cVec3::Reflect( cVec3& aVec )
	{
		float lDot = x*aVec.x + y*aVec.y + z*aVec.z;
		x = x - 2.f * lDot * aVec.x;
		y = y - 2.f * lDot * aVec.y;
		z = z - 2.f * lDot * aVec.z;
	}

};

typedef cVec3 vec3;