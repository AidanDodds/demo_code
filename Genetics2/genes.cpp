#include "genes.h"

namespace Gene
{
    static float eval    ( sGene &a, int node );
    static void  optimize( sGene &a, int node );
};

// generate random gene
void Gene::generate( sGene & a )
{
}

// pull from 'b' into 'a'
void Gene::pullIn( sGene & a, sGene b )
{
}

float Gene::eval( sGene &a, int node )
{

}

//
float Gene::eval( sGene &a, float *x, int nd )
{
    for ( int i=0; i<nd && i <nInputs; i++ )
        a.inputs[ i ] = x[ i ];

    return eval( a, 0 );
}

//
void Gene::jit( sGene &a )
{
    /* not yet */
}

//
void Gene::optimize( sGene &a )
{

}

//
void Gene::dump( sGene &a )
{
}