#pragma once

typedef unsigned char byte;

namespace Gene
{

    const int nInputs = 16;
    const int nNodes  = 32;

    struct sGene
    {
        // must store costants in nodes?
        float inputs[ nInputs ];
        byte  nodes [ nNodes  ];
    };

    namespace eOpcode { enum
    {
        // < 0x80 is input index
        ADD = 0x80  ,   
        SUB         , 
        MUL         ,
        DIV         ,
        ABS         ,
        SIN         , 
    }; }

    // generate random gene
    void generate( sGene & a );

    // pull from 'b' into 'a'
    void pullIn( sGene & a, sGene b );

    //
    float eval( sGene &a, float *x, int nd );

    //
    void jit( sGene &a );

    //
    void optimize( sGene &a );

    //
    void dump( sGene &a );
}