/*  */
void setCameraDir( float aDir, float aLoft )
{
	/*  */
	cVec3  z( aDir, aLoft );
		   z.Normalize( );
	cVec3  y(-z.y*z.x, 1-z.y*z.y,-z.y*z.z );
	       y.Normalize( );
	cVec3  x( &z, &y );
	/* put (x, y, z) into matrix gCamMat */
	float *m = gCamMat;
	m[0x0]=x.x; m[0x1]=x.y; m[0x2]=x.z; 
	m[0x3]=y.x; m[0x4]=y.y; m[0x5]=y.z; 
	m[0x6]=z.x; m[0x7]=z.y; m[0x8]=z.z;
}

/* set this to the cross of aVec1 and aVec2 */
void cVec3::Cross( cVec3* aVec1, cVec3* aVec2 )
{
	x = aVec1->y*aVec2->z - aVec1->z*aVec2->y;
	y = aVec1->z*aVec2->x - aVec1->x*aVec2->z;
	z = aVec1->x*aVec2->y - aVec1->y*aVec2->x;
}

/* reflect this vector around normal aVec */
void cVec3::Reflect( cVec3* aVec )
{
	float lDot = x*aVec->x + y*aVec->y + z*aVec->z;
	x = x - 2 * lDot * aVec->x;
	y = y - 2 * lDot * aVec->y;
	z = z - 2 * lDot * aVec->z;
}

/*  */
void cVec3::Spherical( float aB, float aL )
{
	float lSa = sin( aB );
	float lCa = cos( aB );
	float lSl = sin( aL );
	float lCl = cos( aL );
	x = lSa * lSl;
	z =-lCa * lSl;
	y = lCl;
}