#pragma once

#include "types.h"

class cTexture
{
public:

	cTexture( void )
		: pixels( nullptr )
		, width ( 0 )
		, height( 0 )
		, glName(-1 )
	{ }

	~cTexture( )
		{ unload( ); }

	bool create   ( int w, int h );
	bool load     ( char *path );
	void bind     ( void );
	void unload   ( void );

	int  getWidth ( void ) { return width;  }
	int  getHeight( void ) { return height; }

	void plot( int x, int y, u32 colour )
	{
		if ( x<0 || y<0 || x>=width || y>=height || pixels==nullptr)
			return;

		pixels[ x + y * width ] = colour;
	}

	bool upload( void );

private:
	
	u32 * pixels;
	int   width;
	int   height;

	u32   glName;

};