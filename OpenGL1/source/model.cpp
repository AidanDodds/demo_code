#include "../glee/GLee.h"

#include "model.h"
#include "parser.h"

#include <math.h>

extern bool mesh_loadObj ( cMesh &mesh, char *path );
extern bool mesh_loadProc( cMesh &mesh, char *path );

void cModel::unload( void )
{
    mesh.vertex.release( );
    mesh.index .release( );
}

// load in an obj file from a file
bool cModel::load( char *path )
{
    unload( );

	cParser str( path );

	str.skipToNext( '.' );
	if ( str.canMatch( "obj" ) )
    {
		if (! mesh_loadObj( mesh, path ) )
			return false;
        // generate normals for objs
	    genNormals( );
	}
	else
    if ( str.canMatch( "proc" ) )
    {
        if (! mesh_loadProc( mesh, path ) )
            return false;
    }
    else
		// return if it exists not
		return false;
    
	// upload to OpenGL
	postLoad( );

	return true;
}

// generate the normals for this triangle
// here triangle winding will be critical
void cModel::genNormals( void )
{
	if ( mesh.vertex.items() == 0 )
		return;

	for ( int i=0; i<mesh.vertex.items( ); i++ )
	{
		sVertex &vert = mesh.vertex[i];
		vert.normal = vec3( 0, 0, 0 );
	}
	
	for ( int i=0; i < mesh.index.items( ); i++ )
	{
		// pick the current face
		sIndex &index = mesh.index[ i ];
		
		// pick the vertices of this face
		sVertex &vt1 = mesh.vertex[ index.v1 ];
		sVertex &vt2 = mesh.vertex[ index.v2 ];
		sVertex &vt3 = mesh.vertex[ index.v3 ];

		// 
		vec3 &v1 = vt1.pos;
		vec3 &v2 = vt2.pos;
		vec3 &v3 = vt3.pos;

		// find vectors along edge of face
		vec3 a = vec3( v1.x-v2.x, v1.y-v2.y, v1.z-v2.z );
		vec3 b = vec3( v3.x-v2.x, v3.y-v2.y, v3.z-v2.z );
		
		// cross product is normal
		vec3 cp = vec3
			(
				a.y*b.z - b.y*a.z,
				a.z*b.x - b.z*a.x,
				a.x*b.y - b.x*a.y
			);
		cp.fastNorm( );
		
		// sum with all normals
		vt1.normal += cp;
		vt2.normal += cp;
		vt3.normal += cp;
	}
	
	// normalize all of the summed normals
	for ( int i=0; i<mesh.vertex.items(); i++ )
		mesh.vertex[i].normal.fastNorm( );
}

void cModel::postLoad( void )
{
    if ( mesh.vertex.items() == 0 ||
         mesh.index .items() == 0 )
        return;

	glGenBuffers( 1, &vbo );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData
	(
		GL_ARRAY_BUFFER,
		mesh.vertex.sizeBytes( ),
		&( mesh.vertex[0] ),
		GL_STATIC_DRAW
	);

	glGenBuffers( 1, &ibo );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ibo );
	glBufferData
	(
		GL_ELEMENT_ARRAY_BUFFER,
		mesh.index.sizeBytes(),
		&( mesh.index[0] ),
		GL_STATIC_DRAW
	);
}

void cModel::draw( void )
{
    if ( mesh.vertex.items() == 0 ||
         mesh.index .items() == 0 )
        return;

	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ibo );
	
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( sVertex ), (void*)0 );

	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( sVertex ), (void*)12 );

	glEnableVertexAttribArray( 2 );
    glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, sizeof( sVertex ), (void*)24 );

	glDrawElements( GL_TRIANGLES, mesh.index.sizeBytes(), GL_UNSIGNED_SHORT, 0 );
}
