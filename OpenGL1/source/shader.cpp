#include "../glee/GLee.h"

#include "shader.h"
#include "log.h"

static
void gl_dumpShaderError( GLuint shader )
{
	TRACE( );

	GLint logSize = 0;
	glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
	if ( logSize <= 0 )
		return;
	char *log = new char [ logSize ];
	glGetShaderInfoLog( shader, logSize, NULL, log );
	log[ logSize-1 ] = '\0';
	LOG( log );
	delete [] log;
}

static
void gl_dumpProgramError( GLuint program )
{
	TRACE( );

	GLint logSize = 0;
	glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize );
	if ( logSize <= 0 )
		return;
	char *log = new char [ logSize ];
	glGetProgramInfoLog( program, logSize, NULL, log );
	log[ logSize-1 ] = '\0';
	LOG( log );
	delete [] log;
}

bool cShader::load( char *vshPath, char *fshPath )
{
	TRACE( );

	GLint status = 0;

	if (! fsh.load( fshPath ) )
	{
		LOG( "unable to load fragment shader" );
		return false;
	}

	if (! vsh.load( vshPath ) )
	{
		LOG( "unable to load vertex shader" );
		return false;
	}

	vsobj = glCreateShader( GL_VERTEX_SHADER );
	glShaderSource ( vsobj, 1, (const GLchar **)&vsh.memory, NULL );
	glCompileShader( vsobj );
	glGetShaderiv  ( vsobj, GL_COMPILE_STATUS, &status );
	if (! status )
	{
		gl_dumpShaderError( vsobj );
		LOG( "failed to compile vertex shader" );
		return false;
	}

	fsobj = glCreateShader( GL_FRAGMENT_SHADER );
	glShaderSource ( fsobj, 1, (const GLchar **)&fsh.memory, NULL );
	glCompileShader( fsobj );
	glGetShaderiv  ( fsobj, GL_COMPILE_STATUS, &status );
	if (! status )
	{
		gl_dumpShaderError( fsobj );
		LOG( "failed to compile fragment shader" );
		return false;
	}

	program = glCreateProgram( );
	glAttachShader( program, vsobj );
	glAttachShader( program, fsobj );

	glBindAttribLocation( program, 0, "vPosition" );
	glBindAttribLocation( program, 1, "vNormal"   );
	glBindAttribLocation( program, 2, "vTexCoord" );

	glLinkProgram( program );

	glGetProgramiv( program, GL_LINK_STATUS, &status );
	if (! status )
	{
		gl_dumpProgramError( program );
		glDeleteProgram( program );
		return false;
	}

	return true;
}

void cShader::bind( void )
{
	glUseProgram( program );
}

u32 cShader::getUniformId( char *name )
{
	return glGetUniformLocation( program, name );
	u32 error = glGetError( );

}

void cShader::setUniform( u32 id, const mat4 &data )
{
	glUniformMatrix4fv( id, 1, GL_FALSE, data.e );
	u32 error = glGetError( );
}

void cShader::setUniform( u32 id, const vec3 &data )
{
}

void cShader::setUniform( u32 id, const int &data )
{
	glUniform1i( id, data );
	u32 error = glGetError( );
}