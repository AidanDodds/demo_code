#pragma once
#include "types.h"

class cBinaryReader
{
public:

	cBinaryReader( void ) :
		_index( 0 ),
		_ptr  ( 0 ),
		_limit( 0 )
	{
	}
	
	cBinaryReader( void *stream, int size ) :
		_index( 0 ),
		_ptr  ( (byte*) stream ),
		_limit( size )
	{
	}
	
	byte read_byte( void )
	{
		return _ptr[ _index++ ];
	}
	
	u16 read_u16( void )
	{
		u16 *x = (u16*)(_ptr + _index);
		_index += sizeof( u16 );
		return *x;
	}
	
	u32 read_u32( void )
	{
		u32 *x = (u32*)(_ptr + _index);
		_index += sizeof( u32 );
		return *x;
	}
	
	float read_float( void )
	{
		float *x = (float*)(_ptr + _index);
		_index += sizeof( u32 );
		return *x;
	}
	
	void seek( int offset )
	{
		_index = offset;
	}
	
	void jump( int offset )
	{
		_index += offset;
	}
	
private:

	int   _index;
	byte *_ptr;
	int   _limit;
	
};
