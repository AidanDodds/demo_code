#pragma once

#include "vec3.h"
#include "mat4.h"

class cCamera
{
	vec3  pos;
	float rotation;
	float elevation;

public:

	cCamera( void )
		: pos      (     )
		, rotation ( 0.f )
		, elevation( 0.f )
	{ }

    void setPosition( vec3 &v )
    {
        pos = v;
    }

	void move  ( float x, float y, float z );
	void rotate( float x, float y );

	void getMatrix( mat4 &matrix );

};