#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "model.h"
#include "fileLoader.h"
#include "parser.h"
#include "list.h"
#include "log.h"

static
void proc_parseVertex( cParser &file, cMesh &mesh )
{
    file.expect( "(" );

    sVertex &v = mesh.vertex.alloc( );

    v.pos.z = file.readFloat( );
    v.pos.x = file.readFloat( );
    v.pos.y = file.readFloat( );

    v.uv.x = file.readFloat( );
    v.uv.y = file.readFloat( );

    v.normal.x = file.readFloat( );
    v.normal.y = file.readFloat( );
    v.normal.z = file.readFloat( );

    file.expect( ")" );
}

static
void proc_parseSurface( cParser &file, cMesh &mesh )
{
    file.expect( "{" );

                    file.skipString( ); // material
    int nVertices = file.readInt( );
    int nIndices  = file.readInt( );
    
    int tIdx = mesh.vertex.items( );

    for ( int i=0; i<nVertices; i++ )
    {
        proc_parseVertex( file, mesh );
    }

    if ( nIndices % 3 != 0 )
        assert(! "indices not mod3" );

    for ( int i=0; i<nIndices; i+=3 )
    {
        sIndex &idx = mesh.index.alloc( );

        idx.v1 = file.readInt( ) + tIdx;
        idx.v2 = file.readInt( ) + tIdx;
        idx.v3 = file.readInt( ) + tIdx;
    }

    file.expect( "}" );
}

static
void proc_parseModel( cParser &file, cMesh &mesh )
{
    file.expect( "{" );
        
                    file.skipString( ); // name
    int nSurfaces = file.readInt( );    // surfaces

    for ( int i=0; i<nSurfaces; i++ )
    {
        proc_parseSurface( file, mesh );
    }

    file.expect( "}" );
}

// here we parse the entire file and load in its attributes
static
void proc_parse( cParser &file, cMesh &mesh )
{
	file.reset( );
	file.skipSymbol( ); // proc file name

    while ( file.canMatch( "model" ) )
    {
        proc_parseModel( file, mesh );
    }
}

extern 
bool mesh_loadProc( cMesh &mesh, char *path )
{
	cFileLoader file( path );
	if (! file.isOpen( ) )
	{
		mesh.release( );
		LOG( "unable to open file" );
		return false;
	}
	
    cParser reader
    (
        file.memory,
        cParser::MODE_SKIP_LINES        |
        cParser::MODE_SKIP_COMMENTS     |
        cParser::MODE_SKIP_WHITESPACE
    );

	proc_parse( reader, mesh );
    
	return true;
}