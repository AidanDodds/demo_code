#include <stdio.h>
#include "fileLoader.h"

bool cFileLoader::load( char *path )
{
	unload( );

	// load the obj file
	FILE *file = NULL;
	fopen_s( &file, path, "rb" );
	if ( file == NULL )
		return false;

	// find the file size
	fseek( file, 0, SEEK_END );
	fpos_t size = 0;
	fgetpos( file, &size );
	bytes = (int)size;
	fseek( file, 0, SEEK_SET );

	// allocate data for this file
	memory = new char[ bytes + 1 ];

	// read in the entire file
	if ( fread( memory, 1, bytes, file ) != bytes )
	{
		delete [] memory;
		memory = NULL;
	}
	
	// add an EOF character
	memory[ bytes ] = '\0';

	// close this file after we are done with it
	fclose( file );
	return (memory != NULL);
}

void cFileLoader::unload( void )
{
	if ( memory != NULL )
		delete [] memory;
	memory = NULL;
	bytes  = 0;
}

bool cFileLoader::isOpen( void )
{
	return ( memory != NULL );
}