#include "../glee/GLee.h"
#include "texture.h"
#include "log.h"

const int invalidName = -1;

bool cTexture::create( int w, int h )
{
	unload( );

	if ( w<=0 || h<=0 )
		return false;

	width  = w;
	height = h;

	int nElements = w * h;

	pixels = new u32 [ nElements ];

	return pixels != nullptr;
}

void cTexture::unload( void )
{
	if ( pixels != nullptr )
		delete [] pixels;
	pixels = nullptr;

	if ( glName != invalidName )
	{
		glDeleteTextures( 1, &glName );
		glName = invalidName;
	}

	width  = 0;
	height = 0;
}

bool cTexture::upload( void )
{
    if ( pixels == nullptr )
        return false;

	if ( glName == invalidName )
	    glGenTextures( 1, &glName );

	if ( glName == invalidName )
		return false;
	
	glBindTexture
    (
        GL_TEXTURE_2D,
        glName
    );

	glTexImage2D
	(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		width,
		height,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		pixels
	);

	if ( glGetError( ) != GL_NO_ERROR )
	{
		unload( );
		LOG( "cTexture::load() Failed!" );
		return false;
	}
    
	glActiveTexture( GL_TEXTURE0 );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ); 
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

	return true;
}

void cTexture::bind( void )
{
	if ( glName == invalidName )
		if (! upload( ) )
			return;
		

	glBindTexture( GL_TEXTURE_2D, glName );
}