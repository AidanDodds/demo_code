#pragma once
#include "fileLoader.h"
#include "types.h"

#include "vec3.h"
#include "mat4.h"

class cShader
{
public:

	bool load  ( char *vsh_path, char *fsh_path );
	void unload( void );
	void bind  ( void );

	u32  getUniformId( char *name );
	void setUniform  ( u32 id, const mat4 &data );
	void setUniform  ( u32 id, const vec3 &data );
	void setUniform  ( u32 id, const int  &data );

private:

	cFileLoader vsh, fsh;
	u32 vsobj, fsobj, program;
};