#pragma once

class cFileLoader
{
public:

	cFileLoader( void ) :
		memory( NULL ),
		bytes ( 0    )
	{
	}

	cFileLoader( char *path ) :
		memory( NULL ),
		bytes ( 0    )
	{
		load( path );
	}

	~cFileLoader( void )
	{
		unload( );
	}

	void unload( void       );
	bool load  ( char *path );
	bool isOpen( void       );

	char *memory;
	int   bytes;
};
