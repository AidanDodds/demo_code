#include "camera.h"
#include "utility.h"
#include <math.h>

void cCamera::move( float x, float y, float z )
{
	mat4 tmp; tmp.identity( );

	// get the camera matrix
	getMatrix( tmp );
	
	// transpose so we get the axes in world space?
	tmp.transpose( );

	// apply 
	pos += tmp.getXVector() * x;
	pos += tmp.getYVector() * y;
	pos += tmp.getZVector() * z;
}

void cCamera::rotate( float x, float y )
{
	float rp = hpi * 0.8f;

	rotation  += x;
	elevation -= y;

	if ( rotation  < -pi ) rotation  +=  pi2;
	if ( rotation  >  pi ) rotation  -=  pi2;
	if ( elevation < -rp ) elevation  = -rp;
	if ( elevation >  rp ) elevation  =  rp;
}

void cCamera::getMatrix( mat4 &matrix )
{
	mat4 rx; rx.rotateX( elevation );
	mat4 ry; ry.rotateY( rotation  );
	
	matrix.translate( pos );
	
	// multiply by elevation before rotation
	matrix = mat4::multiply( ry, matrix );
	matrix = mat4::multiply( rx, matrix );

}