
#include <SDL\SDL.h>
//#include <SDL\SDL_opengl.h>
#include "../glee/GLee.h"

#include "log.h"
#include "shader.h"
#include "model.h"
#include "texture.h"
#include "camera.h"

#include <math.h>

SDL_Surface *surf = NULL;

cShader  shader;
cModel   model;
cTexture texture;
cCamera  camera;

#if 0
bool app_loadTexture( void )
{
    if (! texture.create( 512, 512 ) )
	{
		LOG( "texture.create() Failed!" );
        return false;
	}

    SDL_Surface * tex = SDL_LoadBMP( "data\\Imrod_Diffuse.bmp" );
    if ( tex == nullptr )
        return false;

    u32 * p = (u32*) tex->pixels;

    // create a checkerboard pattern
	for ( int i=0; i<512*512; i++ )
	{
		int x = i % 512;
		int y = i / 512;

        u32 c = ((p[i]&0xff) << 16) | (p[i]&0xff00) | ((p[i]&0xff0000) >> 16);

        // BBGGRR
		texture.plot( x, 512 - y, c );
	}
    
    return true;
}
#else
bool app_loadTexture( void )
{
    if (! texture.create( 512, 512 ) )
	{
		LOG( "texture.create() Failed!" );
        return false;
	}
    // create a checkerboard pattern
	for ( int i=0; i<512*512; i++ )
	{
		int x = i % 512;
		int y = i / 512;

        u32 c = ((x%128<4) || (y%128<4))-1;
            c ^= rand()&0x3f;
            c &= 0xff;
            c |= c<<8;
            c |= c<<8;

        // BBGGRR
		texture.plot( x, 512 - y, c );
	}
    
    return true;
}

#endif

bool app_init( void )
{
	TRACE( );

	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
	{
		LOG( "SDL_Init() Failed!" );
		return false;
	}

	surf = SDL_SetVideoMode( 1024, 768, 32, SDL_OPENGL );
	if ( surf == NULL )
	{
		LOG( "SDL_SetVideMode() Failed!" );
		return false;
	}

	if (! shader.load( "data\\vert.glsl", "data\\frag.glsl" ) )
	{
		LOG( "shader.load() Failed!" );
		return false;
	}

    if (! model.load( "data\\stalwart.proc" ) )
//	if (! model.load( "data\\ImrodTri.obj" ) )
	{
		LOG( "model.load() Failed!" );
		return false;
	}

	if (! app_loadTexture( ) )
	{
		LOG( "texture.create() Failed!" );
	}
    
	SDL_WM_SetCaption( "OpenGL Test", NULL );
	return true;
}

bool app_tick( void )
{
	bool active = true;

	SDL_Event event;
	while ( SDL_PollEvent( &event ) )
	{
		if ( event.type == SDL_QUIT )
			active = false;

		if ( event.type == SDL_KEYDOWN || event.type == SDL_KEYUP )
		
			switch ( event.key.keysym.sym )
			{
			case ( SDLK_ESCAPE ):
			case ( SDLK_q ):
				active = false;
			}
		
	}

	return active;
}

void app_quit( void )
{
	TRACE( );

	if ( surf != NULL )
		SDL_FreeSurface( surf );

	SDL_Quit( );
}

void updateCamera( mat4 &out )
{
	int numKeys = 0;
	Uint8 *keys = SDL_GetKeyState( &numKeys );

    const float sp = 2.0f;

	// forward back
	if ( keys[ SDLK_w ] ) camera.move( 0.f, 0.f, sp);
	if ( keys[ SDLK_s ] ) camera.move( 0.f, 0.f,-sp );

	// strafe
	if ( keys[ SDLK_a ] ) camera.move(-sp, 0.f, 0.f );
	if ( keys[ SDLK_d ] ) camera.move( sp, 0.f, 0.f );

	// strafe
	if ( keys[ SDLK_r ] ) camera.move( 0.f,-sp, 0.f ); // up
	if ( keys[ SDLK_f ] ) camera.move( 0.f, sp, 0.f ); // down

	// get mouse deltas
	int rx=0, ry=0;
	SDL_GetRelativeMouseState( &rx, &ry );

	// mouse look
	camera.rotate( ((float)rx)*0.002f, ((float)ry)*0.002f );

	out.identity( );
	camera.getMatrix( out );
}

int main( int argc, char **args )
{
	TRACE( );

	if (! app_init( ) )
		return -1;

	SDL_WM_GrabInput( SDL_GRAB_ON );
	SDL_ShowCursor( SDL_FALSE );
	SDL_WarpMouse( 320, 240 );

	atexit( app_quit );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity( );
	
	glClearColor( 0.1f, 0.2f, 0.3f, 0.4f );

//	glEnable  ( GL_CULL_FACE  );
//  glCullFace( GL_FRONT      ); // this is weird, we must be wound wrong
	glEnable  ( GL_TEXTURE_2D );
	glEnable  ( GL_DEPTH_TEST );

	mat4 matrix;
	mat4 proj;
	matrix.scale( 0.3f );
	proj.projection( 75.0f, 4.0f / 3.0f, 1.0f, 10.0f );
	
	u32 trf_id = shader.getUniformId( "vTransform" );
	u32 prj_id = shader.getUniformId( "vProj"      );
	u32 tex0   = shader.getUniformId( "tex0"       );

    camera.setPosition( vec3( 0.0f,-15.0f,-30.0f ) );

	while ( app_tick( ) )
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		updateCamera( matrix );
		
		texture.bind( );
		shader .bind( );

		shader.setUniform( trf_id, matrix );
		shader.setUniform( prj_id, proj   );
		shader.setUniform( tex0  , 0      );

//		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

		model.draw( );

		SDL_GL_SwapBuffers( );

		SDL_Delay( 10 );
	}

	return 0;
}