#include "types.h"
#include "log.h"
#include <stdarg.h>
#include <stdio.h>

class sFileCloser
{
public:
	 sFileCloser( FILE **file )
	 {
		 _file = file;
	 }
	~sFileCloser( )
	{
		if ( *_file != NULL )
			fclose( *_file );
		*_file = NULL;
	}
private:
	FILE **_file;
};

static cLogTrace   *_traceTip = NULL;
static FILE        *_logFile  = NULL;
static sFileCloser  _logCloser( &_logFile );
static int          _indent   = 0;
static bool			_created  = false;

cLogTrace::cLogTrace( char *_name ) :
	parent ( _traceTip ),
	name   ( _name     ),
	written( false     )
{
	_traceTip = this;
}

cLogTrace::~cLogTrace( void )
{
	_traceTip = parent;
}

void cLogTrace::emmit( void )
{
	if ( parent != NULL )
		parent->emmit( );
	
	fprintf( _logFile, "<%s>", name );
}

bool logIsValid( void )
{
	// create the log file if it doesnt exist
	if ( _logFile == NULL && _created == false )
	{
		fopen_s( &_logFile, "log.txt", "wb" );
		if ( _logFile != NULL )
			_created = true;
	}
	return (_logFile != NULL);
}

void log_write( char *exp, ... )
{
	if (! logIsValid( ) )
		return;

	// print the indentation
	if ( _traceTip != NULL )
		_traceTip->emmit( );
	
	// open argument list
	va_list args;
	va_start( args, exp );
	
	fputc( '\n', _logFile );

	// print this arg list
	vfprintf( _logFile, exp, args );

	// close the argument list
	va_end( args );
	
	// end this line
	fputc( '\n', _logFile );
}