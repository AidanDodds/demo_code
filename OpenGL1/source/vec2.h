#pragma once

struct cVec2
{
    float x, y;

	cVec2( void )
	{ }

	cVec2( float _x, float _y )
		: x( _x )
		, y( _y )
	{ }
};

typedef cVec2 vec2;