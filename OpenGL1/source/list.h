#pragma once
#include <assert.h>

template <typename T>
class cList
{
public:

	cList( void )
		: list    ( nullptr )
		, head    ( 0 )
		, capacity( 0 )
	{
	}

	T & alloc( void )
	{
		if ( capacity == head )
			resize( capacity+1 );
		assert( head < capacity );
		return list[ head++ ];
	}

	T & alloc( int & index )
	{
		index = head;
		return alloc( );
	}

	T & operator [] ( const int index )
	{
		assert( index >= 0 && index < head );
		return list[ index ];
	};

	int items    ( void ) { return head; }
	int sizeBytes( void ) { return head * sizeof( T ); }

	void release( void )
	{
		if ( list != nullptr )
			delete [] list;

		list     = nullptr;
		head     = 0;
		capacity = 0;
	}

	// ensure the list has this many elements availible
	void reserve( int size )
	{
		resize( size );
	}

private:

	void resize( int min=128 )
	{
		capacity += (capacity == 0);
		while ( capacity < min )
			capacity *= 2;

		T *newList = new T[ capacity ];
		assert( newList != nullptr );

		if ( list == nullptr )
		{
			head = 0;
			list = newList;
		}
		else
		{
			// copy over old buffer
			byte *src = (byte*) list;
			byte *dst = (byte*) newList;
			for ( unsigned int i = 0; i < (head*sizeof(T)); i++ )
				dst[i] = src[i];
			
			delete [] list;
			list = newList;
		}
	}

	T   *list;		// list of items
	int  head;		// current list head
	int  capacity;	// size of allocated space

};