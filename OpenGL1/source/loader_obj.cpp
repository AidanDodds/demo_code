#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "model.h"

#include "fileLoader.h"
#include "parser.h"
#include "list.h"

#include "log.h"

struct sVertRecord
{
	// obj file indices
	int vi, ti;
	// vertex index in the mesh structure
	int mi;
	// chaining in the hash list
	sVertRecord *next;
};

const int vhashlistsize = 0x400;
struct sVertRecord *vhashlist[ vhashlistsize ];

struct sObjFile
{
	struct sFace
	{
		sFace( )
			: v1(0), v2(0), v3(0)
			, u1(0), u2(0), u3(0)
		{ }

		int v1, v2, v3;
		int u1, u2, u3;
	};

	cList<vec3>  pos;
	cList<vec2>  uv;
	cList<sFace> face;
};

static
void obj_clearHashList( void )
{
	for ( int i=0; i<vhashlistsize; i++ )
	{
		sVertRecord *vrec = vhashlist[ i ];

		for ( ; vrec != nullptr ;  )
		{
			sVertRecord *del = vrec;
			vrec = vrec->next;
			delete del;
		}

		vhashlist[ i ] = nullptr;
	}
}

static
int obj_addVertex( cMesh &mesh, sObjFile &obj, int vi, int ti )
{
	//todo: search to see if this vertex has been added before

	// hash this vertex for quick search
	int hash  = (vi * 4639) ^ (ti * 1429);
		hash ^= hash >> 12;
		hash %= vhashlistsize;

	sVertRecord *vrec = vhashlist[ hash ];

	// check for existing vertex
	for ( ; vrec != nullptr ; vrec=vrec->next )
	{
		if ( vrec->vi != vi ) continue;
		if ( vrec->ti != ti ) continue;
		return vrec->mi;
	}
	// we must create a new vertex
	{
		int mi = 0;
		sVertex &vert = mesh.vertex.alloc( mi );

		vert.pos = obj.pos[ vi ];
		vert.uv  = obj.uv [ ti ];

		vrec       = new sVertRecord;
		vrec->mi   = mi;
		vrec->ti   = ti;
		vrec->vi   = vi;
		vrec->next = vhashlist[ hash ];
		vhashlist[ hash ] = vrec;

		return mi;
	}
}

// load face data
//
// "f %d/%d/%d %d/%d/%d %d/%d/%d\n", v1_idx, v1_uvs, v1_nrm, v2...
static
void obj_readVertexFace( cParser &file, sObjFile::sFace &face )
{
	face.v1 = file.readInt( )-1;
	file.expect( '/' );
    face.u1 = file.readInt( )-1; file.skipToNext( ' ' );

	face.v2 = file.readInt( )-1;
	file.expect( '/' );
    face.u2 = file.readInt( )-1; file.skipToNext( ' ' );

	face.v3 = file.readInt( )-1;
	file.expect( '/' );
    face.u3 = file.readInt( )-1;
}

// read in a vertex as three floats in a row
static
void obj_readVertexPos( cParser &file, vec3 &pos )
{
	pos.x = file.readFloat( );
	pos.y = file.readFloat( );
	pos.z = file.readFloat( );
}

static
void obj_readVertexUV( cParser &file, vec2 &uv )
{
	uv.x = file.readFloat( );
	uv.y = file.readFloat( );
}

// here we parse the entire file and load in its attributes
static
void obj_parse( cParser &file, sObjFile &obj )
{
	file.reset( );
	
	while (! file.atEOF( ) )
	{
		file.skipToNext( '\n' );

		if ( file.canMatch( "v " ) )
		{
			obj_readVertexPos( file, obj.pos.alloc( ) );
			continue;
		}
		if ( file.canMatch( "f " ) )
		{
			obj_readVertexFace( file, obj.face.alloc( ) );
			continue;
		}
        if ( file.canMatch( "vt " ) )
		{
			obj_readVertexUV( file, obj.uv.alloc( ) );
			continue;
		}
	}
}

extern 
bool mesh_loadObj( cMesh &mesh, char *path )
{
	cFileLoader file( path );
	if (! file.isOpen( ) )
	{
		mesh.release( );
		LOG( "unable to open file" );
		return false;
	}
	
	cParser reader( file.memory );

	sObjFile objFile;
	obj_parse( reader, objFile );

	mesh.index.reserve( objFile.face.items() );

	for ( int i=0; i<objFile.face.items( ); i++ )
	{
		sObjFile::sFace & face  = objFile.face[ i ];
		sIndex          & index = mesh.index.alloc( );

		index.v1 = obj_addVertex( mesh, objFile, face.v1, face.u1 );
		index.v2 = obj_addVertex( mesh, objFile, face.v2, face.u2 );
		index.v3 = obj_addVertex( mesh, objFile, face.v3, face.u3 );
	}

	obj_clearHashList( );

	return true;
}