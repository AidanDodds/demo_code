#pragma once
#include "types.h"

class cParser
{
public:

    enum
    {
        MODE_SKIP_LINES         = 1,
        MODE_SKIP_COMMENTS      = 2,
        MODE_SKIP_WHITESPACE    = 4
    };

	cParser( void *stream, int mode = MODE_SKIP_WHITESPACE )
        : _ptr   ( (byte*) stream )
        , _origin( (byte*) stream )
        , mode( mode )
	{ }

    void setMode( int flag )
    {
        mode = flag;
    }

	bool atEOF( void )
	{
		return *_ptr == '\0';
	}

	void reset( void )
	{
		_ptr = _origin;
	}

	bool  canMatch       ( char *string );

	void  skipFluff      ( void );
	void  skipLine       ( void );
	void  skipToNext     ( char ascii );
    void  skipSymbol     ( char *extra=""   );
    void  skipString     ( char *extra="/." );

	void  expect         ( char ascii );
    void  expect         ( char *string );

	int   readInt  ( bool skipws=true );
	float readFloat( bool skipws=true );

private:

    int mode;

	byte *_origin;
	byte *_ptr;

};