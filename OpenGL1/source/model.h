#pragma once
#include "vec3.h"
#include "vec2.h"
#include "list.h"

struct sIndex
{
	u16 v1, v2, v3;
};

struct sVertex
{
	vec3 pos;
	vec3 normal;
	vec2 uv;
};

struct cMesh
{
	cMesh( void ) :
		index ( ),
		vertex( )
		{ }
	
	~cMesh( )
	    { release( ); }

	void release( void )
	{
		index .release( );
		vertex.release( );
	}
	
	cList<sIndex > index;
	cList<sVertex> vertex;
};

class cModel
{
public:

    void unload( void );
	bool load  ( char *path );
	void draw  ( void );

protected:
	
	void genNormals( void );
	void postLoad  ( void );

	u32 vbo;
	u32 ibo;

public:

	cMesh mesh;
};