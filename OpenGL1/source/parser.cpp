#include "parser.h"
#include <assert.h>

bool cParser::canMatch( char *string )
{
    skipFluff( );

	byte *x = _ptr;
	for ( ;; )
	{
		if ( *string == '\0' )
		{
			_ptr = x;
			return true;
		}

		if ( *x == '\0' )		break;
		if ( *x != *string )	break;

		x++;
		string++;
	}
	return false;
}

void cParser::skipFluff( void )
{
    int commentLevel = 0;
    
    for ( ; _ptr!='\0' ; _ptr++ )
    {
        char ch = *_ptr;

        if ( mode & MODE_SKIP_COMMENTS )
        {
            // enter a block comment /*
            if (( ch == '/' ) && ( _ptr[1] == '*' ))
            {
                _ptr++;
                commentLevel++;
                continue;
            }
            
            // exit a block comment */
            if (( ch == '*' ) && ( _ptr[1] == '/' ))
            {
                _ptr++;
                commentLevel--;
                continue;
            }

            if ( commentLevel > 0 )
                continue;
        }

        if ( mode & MODE_SKIP_LINES )
        {
            if ( ch == '\n' || ch == '\r' ) 
                continue;
        }

        if ( mode & MODE_SKIP_WHITESPACE )
        {
	        if ( ch == ' ' || ch == '\t' )
		        continue;
        }

        break;
    }
}

void cParser::skipLine( void )
{
	for ( ; _ptr != '\0' ; _ptr++ )
		if (*_ptr == '\n' )
		{
			_ptr++;
			return;
		}
}

void cParser::skipString( char *extra )
{
    skipFluff( );
    expect( "\"" );
    skipSymbol( extra );
    expect( "\"" );
}

void cParser::skipToNext( char ascii )
{
	for ( ;*_ptr != '\0'; _ptr++ )
	{
		if ( *_ptr == ascii )
		{
			_ptr++;
			return;
		}
	}
}

void cParser::skipSymbol( char *extra )
{
    skipFluff( );

    bool first=true;

    for ( ; ; _ptr++,first=false )
    {
        char ch = *_ptr;

        bool valid = false;

        valid |= ( ch >= 'a' && ch <= 'z' );
        valid |= ( ch >= 'A' && ch <= 'Z' );
        valid |= ( ch >= '0' && ch <= '9' )&(!first);
        valid |= ( ch == '_' ); 
        
        for ( int i=0; extra[i]!='\0'; i++ )
        {
            if ( ch == extra[i] )
            {
                valid = true;
                break;
            }
        }
        
        if (! valid )
            break;
    }
}

void cParser::expect( char ascii )
{
	if ( *_ptr == ascii )
		_ptr++;
	else
		assert(! "unexpected item" );
}

void cParser::expect( char *_str )
{
    skipFluff( );
    int i=0;
    for ( ;; i++ )
    {
        if ( _str[i] == '\0'    ) break;
        if ( _str[i] != _ptr[i] )
        {
            assert(! "match cant be made" );
        }
    }
    _ptr += i;
}

int cParser::readInt( bool skipws )
{
	if ( skipws )
		skipFluff( );

	int value = 0;

	for ( ; ; _ptr++ )
	{
		char c = *_ptr;
		if ( c >= '0' && c <= '9' )
			value = (value * 10) + (c - '0');
		else
			break;
	}

	return value;
}

float cParser::readFloat( bool skipws )
{
	if ( skipws )
		skipFluff( );

	int  invert = 1;
	s64  value  = 0;
	u64  scale  = 0;
	if ( *_ptr == '-' ) { invert=-1; _ptr++; }

	for ( ; ; _ptr++ )
	{
		char c = *_ptr;
		if ( c >= '0' && c <= '9' )
		{
			value  = (value * 10) + (c - '0');
			scale *= 10;
			continue;
		}
		if ( c == '.' && scale == 0 )
		{
			scale = 1;
			continue;
		}
        if ( c == 'e' && _ptr[1] == '-' )
        {
            int exp = _ptr[2] - '0';
            for ( int i=0; i<exp; i++ )
				scale = (scale * 10);
            _ptr += 2;
            continue;
        }
		break;
	}

    double v = ((double)(value)) * invert;
        
	if ( scale == 0 )
		return (float) v;
		return (float)(v / ((double)scale));
}
