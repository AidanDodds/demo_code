#pragma once

class cLogTrace
{
public:
	 cLogTrace( char *name );
	~cLogTrace( void );

	// ask this node to emmit itself
	void emmit( void );
private:
	cLogTrace *parent;
	char      *name;
	bool       written;
};

// write string to the log
extern void log_write( char *exp, ... );

#ifdef _MSC_VER
#define TRACE( ) cLogTrace __trace( __FUNCTION__ )
#else
#define TRACE( ) cLogTrace __trace( __func__ )
#endif

#define LOG( X, ... ) log_write( X, __VA_ARGS__ )