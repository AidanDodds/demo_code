
uniform sampler2D tex0;

varying vec3 sNormal;
varying vec2 sUV;

void main()
{
	float l = clamp( (sNormal.y + 2.0f) * 0.33f, 0.0f, 1.0f );

	// stored as rgba
	gl_FragColor = vec4( texture2D( tex0, sUV ) ) * l;
}
