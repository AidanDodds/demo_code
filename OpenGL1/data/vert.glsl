

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

uniform mat4 vTransform;
uniform mat4 vProj;

varying vec3 sNormal;
varying vec2 sUV;

void main()
{
	sUV  	    =  vTexCoord;
	gl_Position = (vPosition * vTransform) * vProj;
	sNormal     = vNormal;
}