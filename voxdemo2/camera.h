#pragma once

#define GLM_FORCE_RADIANS
#include "types.h"
#include "glm/mat4x4.hpp"

struct sCamera
{
    glm::mat4 viewMatrix;
    glm::mat4 projMatrix;

    // fov in radians
    void setProjection( float aspect, float fov );

    // set the camera given a view direction and position
    void set( const glm::vec3 &viewDir, const glm::vec3 &viewPos );

    // move the camera first person style
    void move( const glm::vec3 &dir );

/*
    // find ray from the camera into the world
    bool screenToRay( const glm::vec2 &scoord, ray3 & ray );
 */

};