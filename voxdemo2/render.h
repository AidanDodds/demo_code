#pragma once

#define GLM_FORCE_RADIANS
#include "types.h"
#include "camera.h"

#include "glm\vec4.hpp"

// primative vertex buffer
struct cVertexBuffer
{
    int        nColours;
    glm::vec3 *cData;

    int        nVertices;
    glm::vec3 *vData;

    int        nIndices;
    int       *iData;
};

typedef int shader_t;

// rendering subsystem
class cRender
{
    cRender( const cRender & );
    cRender( void );

    // hidden data implementation
    struct sRenderData * data;

public:
    
    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cRender & inst( void )
    {
        static cRender instance;
        return instance;
    }

    //
    // instance methods
    //

    //
    void draw_clear( void );

    // draw a vertex buffer
    void draw_buffer
        ( cVertexBuffer *buffer );
    
    //
    // shaders
    //

    // compile a shader
    bool shader_compile
        ( const char *vsh, const char *fsh, resource_t & handle );

    // bind a shader
    bool shader_bind
        ( resource_t id );

    // get the location of a uniform
    bool shader_getUniformLocation
        ( resource_t handle, const char *name, uniform_t &loc );

    // set a uniform value
    bool shader_setUniform
        ( resource_t handle, uniform_t loc, const glm::mat4 & );

    bool shader_setUniform
        ( resource_t handle, uniform_t loc, const glm::vec4 & );

    //
    // textures
    //
    
    resource_t texture_load
        ( const char *path );

    bool texture_bind
        ( resource_t id );
    
};