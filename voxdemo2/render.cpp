#include "render.h"
#include "resource.h"

#include <glee/GLee.h>
#pragma comment( lib, "opengl32.lib" )

#include <stdio.h>

#define GLM_FORCE_RADIANS
#include "glm\vec4.hpp"
#include "glm\gtc\type_ptr.hpp"

//
struct sRenderData
{
    sRenderData( )
        : camera( nullptr )
    { }

    glm::mat4 *camera;
};

//
struct sShader
{
    GLuint vshader;
    GLuint fshader;
    GLuint program;
};

//
cRender::cRender( void )
    : data( nullptr )
{
}

//
bool cRender::start( void )
{
    assert( data == nullptr );
    data = new sRenderData( );
    assert( data != nullptr );

    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_COLOR_ARRAY );
    glEnableClientState( GL_INDEX_ARRAY );

    glDisable( GL_CULL_FACE );

    return glGetError() == GL_NO_ERROR;
}

//
bool cRender::stop ( void )
{
    return true;
}

//
void cRender::draw_buffer( cVertexBuffer *buffer )
{
    assert( buffer != nullptr );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) buffer->vData );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) buffer->cData );

    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    glIndexPointer( GL_INT, 0, (GLvoid*) buffer->iData );

    glDrawArrays( GL_TRIANGLES, 0, 3 );
}

//
void cRender::draw_clear( void )
{
    glClearColor( .2f, .4f, .7f, .0f );
    glClear( GL_COLOR_BUFFER_BIT );
}

// GL program type
enum
{
    SHADER_VERTEX   = 0,
    SHADER_FRAGMENT ,
    SHADER_PROGRAM
};

//
static
bool dumpShaderError( GLint shader, int type )
{
    GLint logSize = 0;
    char *log = nullptr;
    // switch based on shader type
    switch ( type )
    {
    case ( SHADER_VERTEX   ):
    case ( SHADER_FRAGMENT ):
        {
	        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
	        if ( logSize <= 0 )
		        return false;
	        log = new char [ logSize ];
            glGetShaderInfoLog( shader, logSize, NULL, log );
        }
        break;
    case ( SHADER_PROGRAM ):
        {
            glGetProgramiv( shader, GL_INFO_LOG_LENGTH, &logSize );
            if ( logSize <= 0 )
                return false;
            log = new char [ logSize ];
            glGetProgramInfoLog( shader, logSize, NULL, log );
        }
        break;
    };
    // write trailing zero
    log[ logSize-1 ] = '\0';

    // for now just dump to printf
    printf_s( "%s", log );

    delete [] log;
    return glGetError() == GL_NO_ERROR;
}

// 
bool cRender::shader_compile( const char *vsh, const char *fsh, resource_t &handle )
{
    GLint status = 0;
    
    // compile fragment shader
    GLint fsobj = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( fsobj, 1, (const GLchar **)&fsh, nullptr );
    glCompileShader( fsobj );
    glGetShaderiv( fsobj, GL_COMPILE_STATUS, &status );
    if (! status )
    {
        dumpShaderError( fsobj, SHADER_FRAGMENT );
        return false;
    }

    // compile vertex shader
    GLint vsobj = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( vsobj, 1, (const GLchar **)&vsh, nullptr );
    glCompileShader( vsobj );
    glGetShaderiv( vsobj, GL_COMPILE_STATUS, &status );
    if (! status )
    {
        dumpShaderError( vsobj, SHADER_VERTEX );
        return false;
    }

    // create overall shader program
    GLint shobj = glCreateProgram( );
    glAttachShader( shobj, vsobj );
    glAttachShader( shobj, fsobj );

    // bind fixed attribute locations
	glBindAttribLocation( shobj, 0, "vPosition" );
	glBindAttribLocation( shobj, 1, "vColour"   );

    // try to link the programs
	glLinkProgram( shobj );
    glGetProgramiv( shobj, GL_LINK_STATUS, &status );
    if (! status )
    {
        dumpShaderError( vsobj, SHADER_PROGRAM );
        return false;
    }

    // create a new shader object
    sShader *shader = new sShader( );
    // copy over the shader values
    shader->program = shobj;
    shader->fshader = fsobj;
    shader->vshader = vsobj;
    // add to the resource manager
    shader_t id = cResource<sShader>::inst().add( shader );
    assert( id >= 0 );

    // output the handle
    handle = id;
    
    return glGetError() == GL_NO_ERROR;
}

//
bool cRender::shader_bind( resource_t handle )
{
    sShader *shader = cResource<sShader>::inst().find( handle );
    if ( shader == nullptr )
        return false;

    glUseProgram( shader->program );

    return glGetError() == GL_NO_ERROR;
}

//
bool cRender::shader_getUniformLocation( resource_t handle, const char *name, uniform_t &loc )
{
    sShader *shader = cResource<sShader>::inst().find( handle );
    if ( shader == nullptr )
        return false;

    loc = glGetUniformLocation( shader->program, (const GLchar*) name );

    // name does not orrespond to an active uniform variable
    if ( loc == -1 )
        return false;

    return glGetError( ) == GL_NO_ERROR;
}

//
bool cRender::shader_setUniform( resource_t handle, uniform_t loc, const glm::mat4 &mat )
{
    sShader *shader = cResource<sShader>::inst().find( handle );
    if ( shader == nullptr )
        return false;
    
    glUniformMatrix4fv( loc, 1, GL_FALSE, glm::value_ptr( mat ) );

    return glGetError( ) == GL_NO_ERROR;
}

//
bool cRender::shader_setUniform( resource_t handle, uniform_t loc, const glm::vec4 &v )
{
    sShader *shader = cResource<sShader>::inst().find( handle );
    if ( shader == nullptr )
        return false;

    glUniform4f( loc, v.x, v.y, v.z, v.w );

    return glGetError( ) == GL_NO_ERROR;
}
