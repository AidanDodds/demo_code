#define GLM_FORCE_RADIANS
#include "glm/vec3.hpp"
#include "glm/geometric.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "camera.h"

//
void sCamera::setProjection( float aspect, float fov )
{
    projMatrix = glm::perspective( fov, aspect, 1.f, 1024.0f );
}

// set the camera given a view direction and position
void sCamera::set( const glm::vec3 &az, const glm::vec3 &p )
{
    // normalize z componant
    glm::vec3 z = glm::normalize( az );

    // calculate y vector
//  glm::vec3 d = glm::vec3( 0.f, 1.f, 0.f );
//  glm::vec3 y = d - (d * dir) * dir;
    glm::vec3 y = glm::vec3( -z.y * z.x, 1 - z.y * z.y, -z.y * z.z );

    // calculate x vector
    glm::vec3 x = glm::cross( y, z );

    // pack these vectors into the view matrix
    viewMatrix = glm::mat4
        ( 
            glm::vec4( x, 1.f ),
            glm::vec4( y, 1.f ),
            glm::vec4( z, 1.f ),
            glm::vec4( p, 1.f )
        );
}

// move the camera first person style
void sCamera::move( const glm::vec3 &dir )
{
    viewMatrix = glm::translate( viewMatrix, dir );
}