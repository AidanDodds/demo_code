#pragma once

#include "types.h"

enum
{
    INVALID_RESOURCE_ID = -1
};

// resource manager to decouple ownership of objects
template <class T>
class cResource
{
    static const int nItems = 32;
    T   *data[ nItems ];
    int  refs[ nItems ];

    cResource( const cResource & );

    cResource( void )
    {
        for ( int i=0; i<nItems; i++ )
        {
            refs[ i ] = 0;
            data[ i ] = nullptr;
        }
    }

public:

    static cResource &inst( void )
    {
        static cResource instance;
        return instance;
    }

    // add a resource to the system
    resource_t add( T *item )
    {
        assert( item != nullptr );
        for ( int i=0; i<nItems; i++ )
        {
            if ( refs[ i ] > 0 )
                continue;
            data[ i ] = item;
            refs[ i ] = 1;
            return (resource_t)i;
        }
        return INVALID_RESOURCE_ID;
    }

    T * find( resource_t id )
    {
        assert( id>=0 && id<nItems );
        assert( data[ id ] != nullptr );
        assert( refs[id] > 0 );
        return data[ id ];
    }
    
    void release( resource_t id )
    {
        assert( id>=0 && id<nItems );
        assert( data[ id ] != nullptr );
        assert( refs[ id ] > 0 );
        refs[ id ]--;
    }

    void aquire( resource_t id )
    {
        assert( id>=0 && id<nItems );
        assert( data[ id ] != nullptr );
        assert( refs[ id ] > 0 );
        refs[ id ]++;
    }

};
