#include "framework.h"
#include "utility.h"

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

#pragma comment( lib, "SDL2.lib" )

// data implementation
struct sFrameworkData
{
    sFrameworkData( void )
        : willQuit  ( false )
        , window    ( nullptr )
        , glContext ( nullptr )
    { }

    bool           willQuit;
    SDL_Window    *window;
    SDL_GLContext  glContext;
};

// constructor
cFramework::cFramework( void )
    : data( nullptr )
{ }

//
bool cFramework::start( void )
{
    assert( data == nullptr );
    data = new sFrameworkData( );
    assert( data != nullptr );

    SDL_SetMainReady( );

    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
        return false;

    return true;
}

//
bool cFramework::stop ( void )
{
    assert( data != nullptr );

    // destroy the opengl context
    if ( data->glContext != nullptr )
    {
        SDL_GL_DeleteContext( data->glContext );
        data->glContext = nullptr;
    }

    // destroy the window
    if ( data->window != nullptr )
    {
        SDL_DestroyWindow( data->window );
        data->window = nullptr;
    }

    // delete the hidden data structure
    delete data;
    data = nullptr;

    // close sdl library
    SDL_Quit( );

    return true;
}

void doEventPump( sFrameworkData * data )
{
    assert( data != nullptr );
    SDL_Event event;
    while ( SDL_PollEvent( &event ) )
    {
        if ( event.type == SDL_QUIT )
            data->willQuit = true;

        if ( event.type == SDL_KEYDOWN )
        {
            if ( event.key.keysym.sym == SDLK_ESCAPE )
            {
                data->willQuit = true;
                break;
            }
        }
    }
}

bool cFramework::willQuit( void )
{
    assert( data != nullptr );
    doEventPump( data );
    return data->willQuit;
}

bool cFramework::CreateGLWindow( int w, int h, bool fullscreen )
{
    assert( data != nullptr );

    // flags for window creation
    int flags = SDL_WINDOW_OPENGL;
    if ( fullscreen )
        flags |= SDL_WINDOW_FULLSCREEN;

    // create an SDL window
    data->window =
        SDL_CreateWindow
        (
            "voxdemo 2",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            w, h,
            flags
        );
    if ( data->window == nullptr )
        return false;

    // create an opengl context
    data->glContext =
        SDL_GL_CreateContext( data->window );
    if ( data->glContext == nullptr )
        return false;

    return true;
}

void cFramework::flip( void )
{
    assert( data->window );
    SDL_GL_SwapWindow( data->window );

    SDL_Delay( 10 );
}
