#pragma once

#define GLM_FORCE_RADIANS
#include "glm\fwd.hpp"

typedef unsigned int  u32;
typedef unsigned char byte;

/*
typedef float vec2[ 2 ];

// 3d vector
typedef float vec3[ 3  ];

// 4x4 matrix
typedef float mat4[ 16 ];

// 2d rect
typedef float rect2[ 4 ];
*/

// ray
typedef float ray3[ 6 ];

// 
typedef int resource_t;
typedef int uniform_t;

#ifndef assert
// simple assert
# ifdef _DEBUG
#  define assert( X, ... ) { if (! (X) ) __asm { int 3 } }
# else
#  define assert( X, ... )
# endif
#endif