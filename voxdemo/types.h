#pragma once

typedef   signed long long s64;
typedef unsigned long long u64;
typedef   signed int	   s32;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char      byte;

struct sAABB
{
    sAABB( void )
    { }

    sAABB
    (
        float ax1, float ay1, float az1,
        float ax2, float ay2, float az2
    )
        : x1( ax1 ), y1( ay1 ), z1( az1 )
        , x2( ax2 ), y2( ay2 ), z2( az2 )
    { }

    float x1, y1, z1; // minimum
    float x2, y2, z2; // maximum
};