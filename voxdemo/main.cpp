#define _SDL_main_h
#include <SDL/SDL.h>
#include <math.h>
#include <intrin.h>

#pragma comment( lib, "SDL.lib" )

#include "octree.h"
#include "camera.h"

typedef unsigned int u32;

const int resx = 320;
const int resy = 240;

namespace app
{
    static SDL_Surface *screen = nullptr;
    
    inline void plot( int x, int y, u32 c )
    {
         u32 *p = (u32*) screen->pixels;
         p[ x + y * screen->w ] = c;
    }

    void init( void )
    {
	    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		    exit( -1 );
	    screen = SDL_SetVideoMode( resx*2, resy*2, 32, 0 );
	    if ( screen == NULL )
		    exit( -2 );
    }

    bool tick( void )
    {
	    static bool active = true;
        
	    SDL_Event event;
	    while ( SDL_PollEvent( &event ) )
	    {
		    if ( event.type == SDL_QUIT )
			    active = false;

		    if ( event.type == SDL_KEYDOWN )
		    {
			    switch ( event.key.keysym.sym )
			    {
			    case ( SDLK_ESCAPE ):
			    case ( SDLK_q ):
				    active = false;
				    break;
			    }
		    }
	    }

	    return active;
    }
    
    static inline
    void duffs_pixCopy_2x( u32 *s, u32 *d, int w, int l )
    {
	    u32 c = 0;

	    #define PIXCPY2 { c=*s; *(d)=c; *(d+1)=c; *(d+l)=c; *(d+l+1)=c; d+=2; s+=1; }

        int n = (w+7) / 8;
        switch( w % 8 )
        {
           case 0: do { PIXCPY2;
           case 7:      PIXCPY2;
           case 6:      PIXCPY2;
           case 5:      PIXCPY2;
           case 4:      PIXCPY2;
           case 3:      PIXCPY2;
           case 2:      PIXCPY2;
           case 1:      PIXCPY2;
            } while ( --n > 0 );
        }
    }

    static u32 video[ resx*resy ];

    extern
    void flip( void )
    {
        u32* src = video;
	    u32* dst = (u32*) app::screen->pixels;

	    for ( u32 y=0; y < resy; y++ )
	    {
		    // copy one scanline with duffs device
		    duffs_pixCopy_2x( src, dst, resx, resx*2 );
		    // move on to next row
		    src += resx;
		    dst += resx * 4;
	    }
        
        memset( video, 0x1f1f1f1f, sizeof( video ) );
        SDL_Flip( app::screen );
    }

};

int main( int argc, char **args )
{
    const float pi2 = 2.0f * 3.14159265359f;

	app::init( );
    
    octree_t octree;
    camera_t camera;

    octree.create( 8 );

    //
    float rot = 0.f;

    // 
    uint32_t u = 0;
    uint64_t a = 0;
    int avg = 0;

	// main loop
	while ( app::tick( ) )
	{

        SDL_FillRect( app::screen, nullptr, 0x101010 );
        
        const float d = 24.f;

        vec3 cam = vec3( sinf( rot )*d+4, sinf( rot )*6.0f+4.f, cosf( rot )*d+4 );
        vec3 fop = vec3( 4.0f, 4.0f, 4.0f );
        camera.set( cam, fop, (float)resx );
        
        uint64_t t = __rdtscp( &u );
        camera.render( octree, resx, resy, app::video );
        a += __rdtscp( &u ) - t;

        if ( avg == 0 )
        {
            a /= 32;
            avg += 32;
            printf( "\r%llu                ", a );
        }
        else
            avg -= 1;

        int num = 0;
        if ( SDL_GetKeyState( &num )[ SDLK_SPACE ] != 0 )
            octree.randomize( );

        int mx, my;
        int b = SDL_GetMouseState( &mx, &my );

        if ( b & SDL_BUTTON_LEFT )
            camera.click( octree, mx/2, my/2 );

        rot += 0.03f;
        if ( rot > pi2 )
            rot -= pi2;

        app::flip( );
	}
	
	SDL_Quit( );
	return 0;
}