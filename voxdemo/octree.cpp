#include "types.h"
#include "octree.h"
#include "raybox.h"
#include <stdlib.h>

namespace nOctree
{
    // order to traverse children
    static byte order[] =
    {
                                 // x y z - ray dir
        7, 6, 5, 4,  3, 1, 2, 0, // - - -            !
        6, 4, 7, 5,  2, 0, 3, 1, // + - -            !
        5, 4, 7, 6,  1, 0, 3, 2, // - - +            !
        4, 5, 6, 7,  0, 1, 2, 3, // + - +            !

        3, 1, 2, 0,  7, 6, 5, 4, // - + -            !
        2, 0, 3, 1,  6, 4, 7, 5, // + + -            !
        1, 0, 3, 2,  5, 4, 7, 6, // - + +            !
        0, 1, 2, 3,  4, 5, 6, 7, // + + +            !

    };

    // -1 if at root
    inline int getParent( int node )
    {
        return (node / 8) - 1;
    }

    inline int getChild( int node, int child )
    {
        return (node+1)*8 + child;
    }

    struct sChunk
    {
        int   size;
        vec3 *vertex;
    };

    struct sNode
    {
        sChunk *buffer;
        u32     data;
    };

    static inline
    sAABB childAABB
    (
        int index,
        const sAABB &b,
        const float mx,
        const float my,
        const float mz
    )
    {
        switch ( index )
        {
        case ( 0 ): return sAABB( b.x1, b.y1, b.z1,     mx  , my  , mz   );
        case ( 1 ): return sAABB( mx  , b.y1, b.z1,     b.x2, my  , mz   );
        case ( 2 ): return sAABB( b.x1, b.y1, mz  ,     mx  , my  , b.z2 );
        case ( 3 ): return sAABB( mx  , b.y1, mz  ,     b.x2, my  , b.z2 );

        case ( 4 ): return sAABB( b.x1, my  , b.z1,     mx  , b.y2, mz   );
        case ( 5 ): return sAABB( mx  , my  , b.z1,     b.x2, b.y2, mz   );
        case ( 6 ): return sAABB( b.x1, my  , mz  ,     mx  , b.y2, b.z2 );
        case ( 7 ): return sAABB( mx  , my  , mz  ,     b.x2, b.y2, b.z2 );
        }

        return sAABB( );
    }

    //
    bool cOctree::raycast__( sAABB &b, const ray3 &ray, int & index, const byte *o )
    {
        // check for intersection
        if (! raybox3( b, ray ) )
            return false;
        
        int si = index;

        if ( root[si].buffer == 0 )
            return false;

        // if this is a leaf then escape
        // 
        // XXX: Why -1 here?
        if ( (si+1)*8 >= nodes-1 )
            return true;

        // find mid points
        float mx = (b.x1 + b.x2) * .5f;
        float my = (b.y1 + b.y2) * .5f;
        float mz = (b.z1 + b.z2) * .5f;

        for ( int i=0; i<8; i++ )
        {
            index = getChild( si, o[i] );
            sAABB q = childAABB( o[i], b, mx, my, mz );
            if ( raycast__( q, ray, index, o ) )
                return true;
        }
        
        index = si;
        return false;
    }

    // const vec3 &s    start point
    // const vec3 &d    direction
    // sRayHit &hit     hit info
    bool cOctree::raycast( const ray3 &ray, sRayHit &hit, u32 &c )
    {
        sAABB bound = aabb;
        int index = -1;

        // find the right front to back order
        int ftbc  = (ray.normal.x > 0.f) << 0;
            ftbc |= (ray.normal.z > 0.f) << 1;
            ftbc |= (ray.normal.y > 0.f) << 2;

        // index the oder array
        byte *o_ = order + (ftbc * 8);

        if ( raycast__( bound, ray, index, o_ ) )
        {
            c = root[ index ].data;
            return true;
        }
        
        return false;
    }

    // const vec3 &s    start point
    // const vec3 &d    direction
    // sRayHit &hit     hit info
    bool cOctree::destroy( const ray3 &ray )
    {
        sAABB bound = aabb;
        int index = -1;

        // find the right front to back order
        int ftbc  = (ray.normal.x > 0.f) << 0;
            ftbc |= (ray.normal.z > 0.f) << 1;
            ftbc |= (ray.normal.y > 0.f) << 2;

        // index the oder array
        byte *o_ = order + (ftbc * 8);

        if ( raycast__( bound, ray, index, o_ ) )
        {
            root[ index ].buffer = nullptr;
            return true;
        }
        
        return false;
    }

    // get the voxel at a location v
    bool cOctree::getVoxel( const vec3 & v, byte &vox )
    {
        return false;
    }

    // 
#if 0
    void cOctree::randomize( void )
    {
        for ( int i=0; i<size; i++ )
        {
            root[ i ].data = rand( ) ^ (rand() << 8);
            
            if ( i > (64 + 8 + 1 ) )
            {
                if ( rand()%10 > 3 )
                    root[ i ].buffer = nullptr;
                else
                    root[ i ].buffer = (sChunk*) 1;
            }
            else
                root[ i ].buffer = (sChunk*) 1;
        }
    }
#else
    void cOctree::randomize( void )
    {
        // number of nodes before leaves
        int gap = 0;
        for ( int i=1; i<power; i*=2 )
            gap += i*i*i;

        //
        int nLeaves = power * power * power;

        float mid = (float)( power / 2 );

        // 
        for ( int i=0; i<nLeaves; i++ )
        {
            int x = ( i          % power) * 16;
            int z = ((i / power) % power) * 8;
            int y = ((i / power) / power) * 8;
        
//            root[ gap + i - 1 ].data   = (x) | (z<<8) | (z<<16);
            root[ gap + i - 1 ].data   = rand( ) ^ (rand() << 8);
            root[ gap + i - 1 ].buffer = (sChunk*)1;
        }
    }
#endif

    //
    bool cOctree::create( int s )
    {
        power = s;

        nodes = 0;
        // find the number of nodes required
        for ( int i=s; i>0; i/=2 )
            nodes += i*i*i;
        
        root = new sNode[ nodes ];
        float fs = (float) s;
        aabb = sAABB( 0.f, 0.f, 0.f, fs, fs, fs );
        
        randomize( );

        return true;
    }

};