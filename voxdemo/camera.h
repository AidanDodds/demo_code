#pragma once

#include "vec3.h"
#include "octree.h"

namespace nCamera
{

class cCamera
{
public:

	cCamera
	(
		void
	);

    void set
    ( 
		const vec3  &_pos       ,
		const vec3  &_fpoint    ,
		const float  _nearPlane
    );

	void render
    (
        octree_t &t,
        int resx,
        int resy,
        u32 *p
    ) const;
    
    void click
    (
        octree_t &o,
        float mx,
        float my
    ) const;

private:

	vec3  origin;
	vec3  normal;
	float nearPlane;

};

};

typedef nCamera::cCamera camera_t;