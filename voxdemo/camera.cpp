#include "camera.h"

namespace nCamera
{

cCamera::cCamera
(
	void
)
{ }

void cCamera::set
( 
	const vec3  &_pos       ,
	const vec3  &_fpoint    ,
	const float  _nearPlane
)
{
    origin    = _pos      ;
    nearPlane = _nearPlane;

    normal = _fpoint - _pos;
	normal.fastNorm( );
}

void cCamera::render( octree_t &o, int resx, int resy, u32 *p ) const
{
	vec3 sx(-normal.z, 0, normal.x );
	vec3 sy = vec3::cross( sx, normal );
    ray3 ray;

	ray.origin = origin;


    octreeHitInfo_t hit;

    for ( int i=0; i<resx*resy; i++ )
    {
        float x = (float)( i % resx ) - (resx/2);
        float y = (float)( i / resx ) - (resy/2);
        
		ray.normal = (sx*x) + (sy*y) + (normal*nearPlane);
//			 ray.normal.fastNorm( );
             		
        o.raycast( ray, hit, p[i] );
    }

};

void cCamera::click( octree_t &o, float mx, float my ) const
{
    mx -= 160;
    my -= 120;

	vec3 sx(-normal.z, 0, normal.x );
	vec3 sy = vec3::cross( sx, normal );
    ray3 ray;

	ray.origin = origin;

    octreeHitInfo_t hit;

	ray.normal = (sx*mx) + (sy*my) + (normal*nearPlane);
    o.destroy( ray );

};

};