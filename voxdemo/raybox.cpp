#include "raybox.h"

enum
{
    RIGHT	= 1,
    LEFT	= 2,
    MIDDLE	= 4,
};

extern inline
bool raybox1
(
    vec3  minB  , // box
    vec3  maxB  ,
    vec3  origin, // ray
    vec3  dir   ,
    vec3 &coord   // hit point
)
{
	bool inside = true;

	char quadrant[3];
	int  whichPlane;

	vec3 maxT;
	vec3 candidatePlane;

	// Find candidate planes; this loop can be avoided if
   	// rays cast all from the eye(assume perpsective view)
	for ( int i=0; i < 3; i++ )
    {
		if (origin[i] < minB[i])
        {
			quadrant[i] = LEFT;
			candidatePlane[i] = minB[i];
			inside = false;
		}
        else if (origin[i] > maxB[i])
        {
			quadrant[i] = RIGHT;
			candidatePlane[i] = maxB[i];
			inside = false;
		}
        else
        {
			quadrant[i] = MIDDLE;
		}
    }

	// Ray origin inside bounding box
	if ( inside )
    {
		coord = origin;
		return true;
	}

	// Calculate T distances to candidate planes
	for ( int i = 0; i < 3; i++ )
    {
		if (quadrant[i] != MIDDLE && dir[i] !=0.)
			maxT[i] = (candidatePlane[i]-origin[i]) / dir[i];
		else
			maxT[i] = -1.;
    }

	// Get largest of the maxT's for final choice of intersection
	whichPlane = 0;
	for ( int i = 1; i < 3; i++ )
    {
		if (maxT[whichPlane] < maxT[i])
			whichPlane = i;
    }

	// Check final candidate actually inside box
	if ( maxT[whichPlane] < 0.f )
        return false;

	for ( int i = 0; i < 3; i++ )
    {
		if (whichPlane != i)
        {
			coord[i] = origin[i] + maxT[whichPlane] *dir[i];
			if (coord[i] < minB[i] || coord[i] > maxB[i])
				return false;
		}
        else
			coord[i] = candidatePlane[i];
    }

	return true;
}

static inline
float max( float a, float b )
{
    return a > b ? a : b;
}

static inline
float min( float a, float b )
{
    return a < b ? a : b;
}

extern inline
bool raybox2
(
    const sAABB  box,
    const sRay3  ray,
          vec3  &hit
)
{
    float t = 0;
    vec3 dirfrac;
    const vec3 &dir = ray.normal;
    const vec3 &org = ray.origin;

    // r.dir is unit direction vector of ray
    float dx = 1.0f / dir.x;
    float dy = 1.0f / dir.y;
    float dz = 1.0f / dir.z;

    // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
    // r.org is origin of ray
    float t1 = (box.x1 - org.x) * dx;
    float t2 = (box.x2 - org.x) * dx;
    float t3 = (box.y1 - org.y) * dy;
    float t4 = (box.y2 - org.y) * dy;
    float t5 = (box.z1 - org.z) * dz;
    float t6 = (box.z2 - org.z) * dz;

    float tmin = max( max( min( t1, t2 ), min( t3, t4 )), min( t5, t6 ));
    float tmax = min( min( max( t1, t2 ), max( t3, t4 )), max( t5, t6 ));

    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
    if ( tmax < 0 )
    {
        t = tmax;
        return false;
    }

    // if tmin > tmax, ray doesn't intersect AABB
    if ( tmin > tmax )
    {
        t = tmax;
        return false;
    }

    t = tmin;

    // find the intersection point
    hit = org + dir * (tmin * 0.999f);

    return true;
}

// intersection test without hit point calculation
extern inline
bool raybox3
(
    const sAABB  box,
    const sRay3  ray
)
{
    const vec3 &dir = ray.normal;
    const vec3 &org = ray.origin;

    float dx = 1.0f / dir.x;
    float dy = 1.0f / dir.y;
    float dz = 1.0f / dir.z;

    float t1 = (box.x1 - org.x) * dx;
    float t2 = (box.x2 - org.x) * dx;
    float t3 = (box.y1 - org.y) * dy;
    float t4 = (box.y2 - org.y) * dy;
    float t5 = (box.z1 - org.z) * dz;
    float t6 = (box.z2 - org.z) * dz;

    float tmin = max( max( min( t1, t2 ), min( t3, t4 )), min( t5, t6 ));
    float tmax = min( min( max( t1, t2 ), max( t3, t4 )), max( t5, t6 ));

    return !( (tmax < 0) || (tmin > tmax) );
}

extern inline
bool rayvox
(
    const vec3 vox,      // min of voxel
    vec3  org, vec3 dir, // ray
    vec3  &h             // hitpoint
)
{
    float t = 0;

    vec3 dirfrac;

    // r.dir is unit direction vector of ray
    float dx = 1.0f / dir.x;
    float dy = 1.0f / dir.y;
    float dz = 1.0f / dir.z;

    // lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
    // r.org is origin of ray
    float t1 = (vox.x       - org.x) * dx;
    float t2 = (vox.x + 1.f - org.x) * dx;
    float t3 = (vox.y       - org.y) * dy;
    float t4 = (vox.y + 1.f - org.y) * dy;
    float t5 = (vox.z       - org.z) * dz;
    float t6 = (vox.z + 1.f - org.z) * dz;

    float tmin = max( max( min( t1, t2 ), min( t3, t4 )), min( t5, t6 ));
    float tmax = min( min( max( t1, t2 ), max( t3, t4 )), max( t5, t6 ));

    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
    if ( tmax < 0 )
    {
        t = tmax;
        return false;
    }

    // if tmin > tmax, ray doesn't intersect AABB
    if ( tmin > tmax )
    {
        t = tmax;
        return false;
    }

    t = tmin;

    // find the intersection point
    h = org + dir * (tmin * 0.999f);

    return true;
}
