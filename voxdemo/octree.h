#pragma once

#include "vec3.h"

typedef unsigned char byte;

namespace nOctree
{

    struct sRayHit
    {
        vec3 hit;
        byte value;
    };

    class cOctree
    {
    private:

        int power;

        // total number of nodes in the octree
        int nodes;
        
        // root node of the octree
        struct sNode *root;

        // maximal aabb of the octree
        sAABB aabb; 

        bool raycast__( sAABB &b, const ray3 &ray, int & index, const byte *order );

    public:

        // create a octree of x^3 items
        bool create( int x );

        // const vec3 &s    start point
        // const vec3 &d    direction
        // sRayHit &hit     hit info
        // unsigned int     colour out
        bool raycast( const ray3 &ray, sRayHit &hit, u32 &c );
        
        // raycast and destroy the voxel we hit
        bool destroy( const ray3 &ray );

        // get the voxel at a location v
        bool getVoxel( const vec3 & v, byte &vox );

        //
        void randomize( void );

    };

};

typedef nOctree::cOctree octree_t;
typedef nOctree::sRayHit octreeHitInfo_t;