#pragma once

#include "types.h"
#include "vec3.h"

bool raybox1
(
    sAABB box   ,
    vec3  origin, // ray
    vec3  dir   ,
    vec3 &coord   // hit point
);

bool raybox2
(
    sAABB box   ,
    sRay3 ray   ,
    vec3 &coord   // hit point
);

bool rayvox
(
    vec3  voxel , // minimum of voxel
    vec3  origin, // ray
    vec3  dir   ,
    vec3 &coord   // hit point
);

// raybox without hit point calculation
extern inline
bool raybox3
(
    const sAABB  box,
    const sRay3  ray
);