#pragma once
#include "types.h"

//
const float hpi = 1.57079632679f; //  90 deg
const float pi  = 3.14159265359f; // 180 deg
const float pi2 = 6.28318530718f; // 360 deg

// generate a signed random number from -1.0f to 1.0f
inline float srandf( void )
{
	static signed long long _seed = 0xFACEBABE;
	const u64 mult = 1103515245;
	const u64 sum  = 12345;
	_seed = _seed * mult + sum;
	float  r = (float)((_seed>>13)&0x1FFFF) / (float)0x10000;
	return r - 1.0f;
}

// fast inverse square root hack
inline float isqrtf( float number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;
 
	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y;                       
	i  = 0x5f3759df - ( i >> 1 );               
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );
    y  = y * ( threehalfs - ( x2 * y * y ) );
	return y;
}

inline float clampf( float min, float x, float max )
{
	if ( x < min ) return min;
	if ( x > max ) return max;
				   return x;
}

inline float maxf( float a, float b )
{
	if ( a > b ) return a;
				 return b;
}

inline float minf( float a, float b )
{
	if ( a < b ) return a;
				 return b;
}
